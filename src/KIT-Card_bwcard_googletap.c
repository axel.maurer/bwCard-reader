// *******************************************************************
// **                                                               **
// ** File: KIT-Card_bwcard_googletap.c                                   **
// ** Date: 21.12.2023                                              **
// **                                                               **
// ** This file was created on basis of                             **
// ** generated as part of a project by:                            **
// ** AppBlaster V4.80                                              **
// **                                                               **
// *******************************************************************

#include "twn4.sys.h"
#include "apptools.h"
#include "KIT-Card.h"
#include "../../KIT-Card_Keys/keys_KIT.h"
// #include "keys.h"
// #include "keysNull.h"

#define CARDTIMEOUT 1000UL   // Timeout in milliseconds
#define MAXCARDIDLEN 32      // Length in bytes
#define MAXCARDSTRINGLEN 256 // Length W/O null-termination
#define SEARCH_BLE(a, b, c, d) false
#define BLE_MASK 0

// *******************************************************************
// ****** Global Variables *******************************************
// *******************************************************************
char mBuffer[256];
char outBufferStr[256];

// ------ Options --------------------------------

const byte AppManifest[] =
    {
        USB_KEYBOARDREPEATRATE, 1, 10,
        USB_KEYBOARDLAYOUT, 1, USB_KEYBOARDLAYOUT_ENGLISH,
        USB_KEYBOARDSENDALTCODES, 1, USB_KEYBOARDSENDALTCODES_OFF,
        USB_SERIALNUMBER, 1, USB_SERIALNUMBER_OFF,
        USB_SUPPORTREMOTEWAKEUP, 1, USB_SUPPORTREMOTEWAKEUP_OFF,
        EXECUTE_APP, 1, EXECUTE_APP_AUTO,
        ENABLE_WATCHDOG, 1, WATCHDOG_ON,
        TLV_END};

// ------ Transponder-Specific Variables ---------
#ifdef DEBUG
#pragma message "Debugging is turned on"
#define DEBUG_STRING(x) hprintf("%s\r", x)
#define DEBUG_PRINTF(f, ...) hprintf(f, __VA_ARGS__)
#define DEBUG_HEX(Bytes, ByteCnt)                      \
    do                                                 \
    {                                                  \
        HostWriteHex(Bytes, ByteCnt * 8, ByteCnt * 2); \
        HostWriteString("\r");                         \
    } while (false)
#define DEBUG_DATALINE(a, b, c)                   \
    do                                            \
    {                                             \
        WriteDataLine(GetHostChannel(), a, b, c); \
        HostWriteString("\r");                    \
    } while (false)
#else
#define DEBUG_STRING(x)
#define DEBUG_PRINTF(f, ...)
#define DEBUG_HEX(Bytes, ByteCnt)
#define DEBUG_DATALINE(a, b, c)
#endif

void WriteOutStr(char *message)
{
#ifdef READER_ACTION
    HostWriteString(message);
#else
    WriteBytes(CHANNEL_USB, (const byte *)message, strlen(message));
#endif
}

void PrintDebug(char *message)
{
#ifdef DEBUG
    WriteOutStr("DEBUG: ");
    WriteOutStr(message);
#endif
}
void PrintLnDebug(char *message, int lineNumber)
{
#ifdef DEBUG
    char mb[256];
    sprintf(mb, "Line: %d: %s", lineNumber, message);
    mb[200] = 0;
    PrintDebug(mb);
    WriteOutStr("\r");
#endif
}

void NDEF_ParseRecord(byte *Buffer, NDEFRecord *record);
void NDEF_CreateRecord(byte TNF, bool MessageBegin, bool MessageEnd, bool ShortRecord, byte *Type, byte TypeLength, byte *ID, byte IDLength, byte *Payload, int PayloadLength, NDEFRecord *record);

bool ReadPassIDGoogleSmartTap(byte *PassID, int *PassIDByteCnt, int MaxPassIDByteCnt)
{
    int PassDataByteCnt;
    byte PassData[128];
    if (!ReadCardDataGoogleSmartTap(PassData, &PassDataByteCnt, sizeof(PassData)))
        return false;
    // DEBUG_DATALINE("PassData", PassData, PassDataByteCnt);
    ConvertBinaryToString(PassData, 0, PassDataByteCnt * 8, outBufferStr, 16, 0, sizeof(outBufferStr));
    sprintf(mBuffer, "PassData: %s,: %d", outBufferStr, PassDataByteCnt);
    PrintLnDebug(mBuffer, __LINE__);
    NDEFRecord record;
    NDEF_ParseRecord(PassData, &record);
    byte *pPass = PassData;
    if (record.TypeLength == 3 && strncmp((const char *)&record.Buffer[record.TypeOffset], "asv", 3) == 0)
    {
        pPass = &record.Buffer[record.PayloadOffset];
        do
        {
            NDEF_ParseRecord(pPass, &record);
            if (record.TypeLength == 2 && strncmp((const char *)&record.Buffer[record.TypeOffset], "ly", 2) == 0)
            {
                pPass = &record.Buffer[record.PayloadOffset];
                do
                {
                    NDEF_ParseRecord(pPass, &record);
                    if (record.TypeLength == 1 && strncmp((const char *)&record.Buffer[record.TypeOffset], "T", 1) == 0)
                    {
                        if (record.PayloadLength == 0)
                            return false;
                        *PassIDByteCnt = MIN(MaxPassIDByteCnt, record.PayloadLength - 1);
                        memcpy(PassID, &record.Buffer[record.PayloadOffset + 1], *PassIDByteCnt);
                        return true;
                    }
                    pPass += record.Length;
                } while (!NDEF_IsMESet(record.Buffer[0]) && pPass < &PassData[PassDataByteCnt]);
            }
            pPass += record.Length;
        } while (!NDEF_IsMESet(record.Buffer[0]) && pPass < &PassData[PassDataByteCnt]);
    }
    return false;
}

// *******************************************************************
// ****** MIFARE DESFire (File) **************************************
// *******************************************************************

bool ReadBwcardApp(char *CardString, int MaxCardStringLen)
{
    // ------ STEP 1: Test and read data from transponder ------------
    byte CardData[256];
    int CardDataBitCnt;
    if (!DESFire_SelectApplication(CRYPTO_ENV0, 0xF58860))
        return false;
    if (!DESFire_Authenticate(CRYPTO_ENV0, 1, BWCARD_READ_KEY, sizeof(BWCARD_READ_KEY), DESF_KEYTYPE_AES, DESF_AUTHMODE_EV1))
        return false;
    if (!DESFire_ReadData(CRYPTO_ENV0, 0x00, CardData, 0, 12, DESF_COMMSET_FULLY_ENC))
        return false;
    CardDataBitCnt = MIN(96, sizeof(CardData) * 8);

    // ------ STEP 2: Do bit manipulation ----------------------------

    // (No bit manipulation specified)

    // ------ STEP 3: Format output data -----------------------------
    int RemainingDigits, MinFieldDigits, MaxFieldDigits, FieldBitCnt;
    int DigitCnt, FillByteCount;
    char *WritePos = CardString;
    *WritePos = 0;
    // ------ Field 1 (ASCII) ----------------------------------------
    RemainingDigits = MaxCardStringLen - strlen(CardString);
    // Output all digits of the field
    MaxFieldDigits = RemainingDigits;
    MinFieldDigits = 0;
    FieldBitCnt = CardDataBitCnt;
    DigitCnt = MIN((FieldBitCnt + 7) / 8, MaxFieldDigits);
    FillByteCount = MAX(MinFieldDigits - DigitCnt, 0);
    FillBytes((byte *)WritePos, '0', FillByteCount);
    WritePos += FillByteCount;
    CopyBits((byte *)WritePos, 0, CardData, 0, DigitCnt * 8);
    WritePos += DigitCnt;
    *WritePos = 0;
    // ---------------------------------------------------------------

    return true;
}

// *******************************************************************
// ****** Google Pay Smart Tap (ID, any length) **********************
// *******************************************************************

bool ReadGoogleTap(char *CardString, int MaxCardStringLen)
{
    // ------ STEP 1: Test and read data from transponder ------------
    byte CardData[256];
    int CardDataBitCnt;
    int CardDataByteCnt;
    if (!ReadPassIDGoogleSmartTap(CardData, &CardDataByteCnt, sizeof(CardData)))
        return false;
    sprintf(mBuffer, "CardData: %s,CardDataByteCnt: %d", CardData, CardDataByteCnt);
    PrintLnDebug(mBuffer, __LINE__);
    CardDataBitCnt = CardDataByteCnt * 8;
    if (CardDataBitCnt == 0)
        return false;

    // ------ STEP 2: Do bit manipulation ----------------------------

    // (No bit manipulation specified)

    // ------ STEP 3: Format output data -----------------------------
    int RemainingDigits, MinFieldDigits, MaxFieldDigits, FieldBitCnt;
    int DigitCnt, FillByteCount;
    char *WritePos = CardString;
    *WritePos = 0;
    // ------ Field 1 (ASCII) ----------------------------------------
    RemainingDigits = MaxCardStringLen - strlen(CardString);
    // Output all digits of the field
    MaxFieldDigits = RemainingDigits;
    MinFieldDigits = 0;
    FieldBitCnt = CardDataBitCnt;
    DigitCnt = MIN((FieldBitCnt + 7) / 8, MaxFieldDigits);
    FillByteCount = MAX(MinFieldDigits - DigitCnt, 0);
    FillBytes((byte *)WritePos, '0', FillByteCount);
    WritePos += FillByteCount;
    CopyBits((byte *)WritePos, 0, CardData, 0, DigitCnt * 8);
    WritePos += DigitCnt;
    *WritePos = 0;
    // ---------------------------------------------------------------

    return true;
}

// *******************************************************************
// ****** Transponder Evaluation Function ****************************
// *******************************************************************

bool ReadCardData(int TagType, char *CardString, int MaxCardStringLen)
{
    if (TagType != HFTAG_MIFARE)
        return false;

    if (ReadBwcardApp(CardString, MaxCardStringLen))
        return true;
    if (ReadGoogleTap(CardString, MaxCardStringLen))
        return true;
    return false;
}

void OnStartup(void)
{
    CompLEDInit(REDLED | YELLOWLED | GREENLED);
    CompLEDOff(REDLED);
    CompLEDOff(YELLOWLED);
    CompLEDOn(GREENLED);
    SetVolume(30);
    BeepLow();
    BeepHigh();
    SetTagTypes(LFTAGTYPES, HFTAGTYPES & ~BLE_MASK);
    const byte cfgParams[] = {
        SUPPORT_CONFIGCARD, 1,
        SUPPORT_CONFIGCARD_ON,
        USB_KEYBOARDREPEATRATE, 1, 1,
#ifdef GERMAN_KEYBOARD
        USB_KEYBOARDLAYOUT, 1, USB_KEYBOARDLAYOUT_GERMAN,
#endif
        TLV_END};

    // Parameter setzen
    SetParameters(cfgParams, sizeof(cfgParams));
    SetParameters(ELATEC_KEY_DATA, sizeof(ELATEC_KEY_DATA));
}

void OnNewCardFound(const char *CardString)
{
    HostWriteString(CardString);
    HostWriteString("\r");
    CompLEDOn(REDLED);
    CompLEDBlink(REDLED, 500, 500);
    CompLEDOff(YELLOWLED);
    CompLEDOff(GREENLED);
    SetVolume(100);
    BeepHigh();
}

void OnCardTimeout(const char *CardString)
{
    CompLEDOff(REDLED);
    CompLEDOff(YELLOWLED);
    CompLEDOn(GREENLED);
}

void OnCardFound(const char *CardString)
{
}

void OnCardDone(void)
{
    ApplePayApp_WaitRemoved();
}


// ******************************************************************
// ****** Main Program Loop *****************************************
// ******************************************************************

int main(void)
{
    OnStartup();


    char OldCardString[MAXCARDSTRINGLEN + 1];
    OldCardString[0] = 0;

    while (true)
    {
        int TagType;
        int IDBitCnt;
        byte ID[32];

        // Check if transponder has changed
        if (!ISO14443_4_CheckPresence())
        {

            // Search a transponder
            if (SearchTag(&TagType, &IDBitCnt, ID, sizeof(ID)) || SEARCH_BLE(&TagType, &IDBitCnt, ID, sizeof(ID)))
            {
                // A transponder was found. Read data from transponder and convert
                // it into an ASCII string according to configuration
                char NewCardString[MAXCARDSTRINGLEN + 1];
                if (ReadCardData(TagType, NewCardString, sizeof(NewCardString) - 1))
                {
                    // Is this a newly found transponder?
                    if (strcmp(NewCardString, OldCardString) != 0)
                    {
                        // Yes. Save new card string
                        strcpy(OldCardString, NewCardString);
                        OnNewCardFound(NewCardString);
                    }
                    // (Re-)start timeout
                    StartTimer(CARDTIMEOUT);
                }
                // Search again for signaling GoogleTap, that reading process finished
                SearchTag(&TagType, &IDBitCnt, ID, sizeof(ID));
            }

            if (TestTimer())
            {
                OnCardTimeout(OldCardString);
                OldCardString[0] = 0;
            }
        }
    }
}
