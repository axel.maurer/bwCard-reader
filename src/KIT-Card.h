#define MAXIDBYTES 16
#define MAXIDBITS (MAXIDBYTES * 8)

#define NO_ACTION 0
#define ACTION_BWCARD_OLD_READ_NUMBER 1
#define ACTION_BWCARD_OLD_READ_NUMBER_UID 2
#define ACTION_BWCARD_OLD_READ_NUMBER_BEEP 3
#define ACTION_UID_DEC_MSB 4
#define ACTION_UID_HEX_DEC_LSB 5
#define ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED 10
#define ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID 11
#define ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID_CARDUID 12
#define ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED_DEVICEUID 13
#define ACTION_BWCARD_OLD_READ_NUMBER_FILE_1 14
#define ACTION_BWCARD_OLD_WRITE_FILE_1 15
#define ACTION_BWCARD_OLD_UID_READ_NUMBER_FILE_1 16
#define ACTION_BWCARD_OLD_3G_CHECK 17
#define ACTION_BWCARD_OLD_3G_CHECK_ENCRYPTED 18
#define ACTION_BWCARD_OLD_CHECK_FILE_1 19

#define ACTION_SUCCESS_BEEP 20
#define ACTION_SUCCESS_BEEP_OUT 21
#define ACTION_SUCCESS_BEEP_3G 22
#define ACTION_SUCCESS_BEEP_WRITE 23
#define ACTION_SUCCESS_BEEP_RET 25
#define ACTION_SUCCESS_BEEP_OUT_RET 26
#define ACTION_SUCCESS_BEEP_3G_RET 27
#define ACTION_SUCCESS_BEEP_WRITE_RET 28

#define ACTION_ERROR_BEEP 30
#define ACTION_HARD_ERROR_BEEP 31
#define ACTION_ERROR_BEEP_RET 35
#define ACTION_HARD_ERROR_BEEP_RET 36

#define ACTION_DEVICEUID 50
#define ACTION_LOGIN_DATA 51
#define ACTION_DEVICEUID_VERSION 52

#define ACTION_BWCARD_READ_NUMBER 60
#define ACTION_BWCARD_READ_NUMBER_ENCRYPTED 61
#define ACTION_BWCARD_READ_NUMBER_DEVICEUID 62
#define ACTION_BWCARD_READ_NUMBER_DEVICEUID_CARDUID 63
#define ACTION_BWCARD_READ_NUMBER_ENCRYPTED_DEVICEUID 64
#define ACTION_BWCARD_READ_ALL 65

#define ACTION_CREATE_OSS_APP 70
#define ACTION_READ_OSS_APP_INFO_FILE 71
#define ACTION_READ_OSS_APP_DATA_FILE 72
#define ACTION_WRITE_OSS_APP_DATA_FILE 73
#define ACTION_KIT_CARD_READ_NUMBER 75
#define ACTION_KIT_CARD_READ_NUMBER_ENCRYPTED 76

#define ACTION_PHFR_CHANGE_FIRMA 80
#define ACTION_WRITE_DORMA_KABA_CARD_ID 81
#define ACTION_READ_FILE_DATA 85

#define ACTION_GOOGLESMARTTAP_READ 90
#define ACTION_GOOGLESMARTTAP_BWCARD_READ 91
#define ACTION_GOOGLESMARTTAP_READ_WAIT 92
#define ACTION_GOOGLESMARTTAP_BWCARD_READ_WAIT 93

#define ACTION_WAIT_FOR_CARD 97
#define ACTION_VERSION 98
#define ACTION_CHECK_FOR_CARD 99

#define ACTION_SECURE_INIT 100
#ifndef NO_SECURE
#define ACTION_SECURE_GET_UID 101
#define ACTION_SECURE_READ_FILE 110
#define ACTION_SECURE_CHECK_FOR_APP 111
#define ACTION_SECURE_LIST_APPS 112
#define ACTION_SECURE_CREATE_APPS 120
#define ACTION_SECURE_DELETE_APPS 121
#define ACTION_SECURE_CHECK_FOR_APP_FILE 122
#define ACTION_SECURE_CREATE_APP_FILE 123
#define ACTION_SECURE_WRITE_FILE 130
#endif
#define LED_BLINK_TIME 1500
#define LED_RESPONSE_TIME 800

// ApplicationIDs auf der DESfire-Karte
#define BWCARD_DESFIRE_APP_FILEID 0
#define WAY2PAY_DESFIRE_APP_FILEID 1
#define OSS_SIPORT_DESFIRE_APP_INFO_FILEID 0
#define OSS_SIPORT_DESFIRE_APP_DATA_FILEID 1
#define OSS_SIPORT_DESFIRE_APP_INFO_MAX_EVENTLIST 10
#define OSS_SIPORT_DESFIRE_APP_INFO_MAX_BLACKLIST 16
#define OSS_DATA_DOORINFO_COUNT 150
#define CLOSED_APP -1
#define APPID_LEN 6
#define FILEID_LEN 2
#define FILESIZE_LEN 3

#define UID_SIZE 7
#define DEVICE_UID_LEN 12
#define PARM_BUFFER_LEN 600
#define MBUFFER_LEN 1500
#define LFTAGTYPES (NOTAG)
#define HFTAGTYPES (ALL_HFTAGS & ~(TAGMASK(HFTAG_NFCP2P)))
#define BLE_MASK 0
#define CONFIGENABLED SUPPORT_UPGRADECARD_ON

#define CHECK_SECONDS 561600

#define KIT 1
#define STB 2
#define BLB 3
#define UFR 4
#define UKN 5
#define UST 6
#define PHFR 7

#define KIT_Classic_sectorNumber 11
#define KIT_Classic_sectorCount 4
#define APP_KEY_COUNT 28
#define KEY_LEN 16
#define MASTER_KEY_INDEX 0xFFFFFF

#define ROOT_ID 0x000000
#define DUMMY_APP 0xa1234b
#define BWCARD_OLD_APP 0xbecaad
#define KIT_CARD_APP 0xcc1580
#define BWCARD_APP 0xf58860
#define WAY2PAY_APP 0x178800
#define CS_PURSE_APP 0x178816
#define OSS_SIPORT_APP 0xF51773
#define KIT_BIB_APP 0xba1580
#define GANTNER_APP 0x0584DF
#define IC05_APP 0x05845F
#define DORMA_KABA_APP 0xF52100

#define IC05_FILE_OEDT 0x03
#define DORMA_KABA_APP_IDFILE_ID 0

#define AUTH_KEY_NO_KEY -1
#define AUTH_KEY_MASTER 0
#define AUTH_KEY_READ 1
#define AUTH_KEY_WRITE 2
#define AUTH_KEY_OTHER_1 3

struct appKey
{
    int appId;
    int oe;
    int keyLen;
    int keyType;
    const byte masterKey[16];
    const byte readKey[16];
    const byte writeKey[16];
    const byte otherKey_1[16];
};

typedef struct
{
    int appId;
    int fileId;
    byte *fileData;
} actionParmType;

typedef struct __attribute__((__packed__))
{
    byte majorVersion;
    byte minorVersion;
    byte cardType;
    byte cardId[10];
    byte maxEventEntries;
    byte maxBlacklistEntries;
    byte reserved[17];
} OssInfoFileData;

typedef struct __attribute__((__packed__))
{
    byte DoorId[2];
    byte DTSchedule;
} OssDataDoorInfo;

typedef struct __attribute__((__packed__))
{
    byte valDateYearPart1;
    byte valDateYearPart2;
    byte valDateMonth;
    byte valDateDay;
    byte valDateHour;
    byte valDateMinute;
    byte siteId[2];
    byte headerReserved0;
    byte headerDTSchedule;
    byte headerDoorIndex;
    byte headerExtension;
    byte headerReserved4[4];
    OssDataDoorInfo ossDataDoorInfo[OSS_DATA_DOORINFO_COUNT];
} OssDataFileData;

typedef struct
{
    int appId;
    int authKey;
} keyApp;

typedef struct __attribute__((__packed__))
{
    byte *Buffer;
    int Length;
    byte TypeLength;
    byte IDLength;
    int PayloadLength;
    int TypeOffset;
    int IDOffset;
    int PayloadOffset;
} NDEFRecord;
