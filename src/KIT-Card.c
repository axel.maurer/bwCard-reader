/* ******************************************************************
 *
 *    Datei:   KIT-Card.c
 *    Datum:   2023-12-21
 *    Autor: Axel Maurer
 *             axel.maurer@kit.edu
 *
 * Version: 1020 auf der Basis der Umcodierstation von A. Naß
 *
 * Ziel:
 * -----
 * Kodieren einer KIT-Card
 *
 *
 * ******************************************************************/

/********************************************************************
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Dieses Programm ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.
 ********************************************************************/

#include <twn4.sys.h>
#include <apptools.h>
#include "KIT-Card.h"
#include "../../KIT-Card_Keys/keys_KIT.h"
// #include "keysNull.h"

#define READER_VERSION 1020

// Globale Variablen:
// ******************
char *eol = "\r\n";
byte secureToken[KEY_LEN];
const byte EMPTY_TOKEN[KEY_LEN] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

int ossDoorInfoLen = sizeof(OssDataDoorInfo);

void NDEF_ParseRecord(byte *Buffer, NDEFRecord *record);
void NDEF_CreateRecord(byte TNF, bool MessageBegin, bool MessageEnd, bool ShortRecord, byte *Type, byte TypeLength, byte *ID, byte IDLength, byte *Payload, int PayloadLength, NDEFRecord *record);

keyApp openedAppGlobal;
char mBuffer[MBUFFER_LEN];
char outBufferStr[MBUFFER_LEN];

unsigned long LedTimerStart;

// Funktionen:
// ***********

/* Fehlerbehandlungsroutine
 * Gibt die Fehlernachricht via Tastatureingabe, piepst und läßt die rote LED blinken.
 */

void PrintError(char *message, char *result)
{
    // HostWriteString(message);
    sprintf(result, "999,Error: %s%s", message, eol);
    LEDOff(GREENLED);
    LEDBlink(REDLED, 50, 50);
}

/* gibt eine Debug-Nachricht via USB aus */

void WriteOutStr(char *message)
{
#ifdef READER_ACTION
    HostWriteString(message);
#else
    WriteBytes(CHANNEL_USB, (const byte *)message, strlen(message));
#endif
}

void PrintDebug(char *message)
{
#ifdef DEBUG
    WriteOutStr("DEBUG: ");
    WriteOutStr(message);
#endif
}
void PrintLnDebug(char *message, int lineNumber)
{
#ifdef DEBUG
    char mb[MBUFFER_LEN];
    sprintf(mb, "Line: %d: %s", lineNumber, message);
    mb[200] = 0;
    PrintDebug(mb);
    WriteOutStr(eol);
#endif
}

/* gibt eine Info-Nachricht via USB aus */
void PrintInfo(char *message)
{
    WriteOutStr(message);
    //  HostWriteString(message);
    //  HostWriteString(eol);
}

void PrintLnInfo(char *message)
{
    PrintInfo(message);
    WriteOutStr(eol);
}
/* Damit man mitkriegt, daß der Reader an und bereit ist, ein bißchen piepsen
 */
void StartBeep(void)
{
    // Etwas piepsen zum Start, nicht ganz so laut:
    // Beep(Volume, Freq, OnTime, OffTime)
#ifndef READER_ACTION
    Beep(50, 392, 75, 20);
    Beep(50, 392, 75, 30);
    Beep(50, 523, 88, 20);
    Beep(50, 523, 88, 30);
#endif
    Beep(50, 659, 75, 20);
    Beep(50, 523, 75, 40);
    Beep(50, 392, 90, 20);
}

int GetKeyIndex(int appId, int oe)
{
    int i = 0;
    while (i < APP_KEY_COUNT)
    {
        if ((appKeys[i].appId == appId) && (appKeys[i].oe == oe))
        {
            return (i);
        }
        i++;
    }
    return (-1);
}

void decStr2Bytes(char *inStr, byte *outBuffer, int outBufferSize)
{
    uint64_t bInt = 0;

    for (int i = 0; i < strlen(inStr); i++)
    {
        bInt = bInt * 10 + (*(inStr + i) - '0');
    }
    outBuffer[outBufferSize - 8] = (bInt >> 56) & 0xFF;
    outBuffer[outBufferSize - 7] = (bInt >> 48) & 0xFF;
    outBuffer[outBufferSize - 6] = (bInt >> 40) & 0xFF;
    outBuffer[outBufferSize - 5] = (bInt >> 32) & 0xFF;
    outBuffer[outBufferSize - 4] = (bInt >> 24) & 0xFF;
    outBuffer[outBufferSize - 3] = (bInt >> 16) & 0xFF;
    outBuffer[outBufferSize - 2] = (bInt >> 8) & 0xFF;
    outBuffer[outBufferSize - 1] = bInt & 0xFF;
}

int Dec2Int(byte *byteBuf, int byteCount)
{
    int bInt = 0;
    for (int i = 0; i < byteCount; i++)
    {
        bInt = bInt * 10 + (byteBuf[i] - '0');
    }
    return (bInt);
}

int Hex2Int(byte *byteBuf, int byteCount)
{
    int bInt = 0;
    for (int i = 0; i < byteCount; i++)
    {
        if ((byteBuf[i] >= 'a') & (byteBuf[i] <= 'f'))
        {
            bInt = bInt * 16 + (byteBuf[i] - 'a' + 10);
        }
        else
        {
            if ((byteBuf[i] >= 'A') & (byteBuf[i] <= 'F'))
            {
                bInt = bInt * 16 + (byteBuf[i] - 'A' + 10);
            }
            else
            {
                bInt = bInt * 16 + (byteBuf[i] - '0');
            }
        }
    }
    return (bInt);
}

int Byte2Int(byte *byteBuf, int byteCount)
{
    const byte prefix[2] = "0x";
    if (CompBytes(byteBuf, prefix, 2))
    {
        return (Hex2Int(&byteBuf[2], byteCount - 2));
    }
    else
    {
        return (Dec2Int(byteBuf, byteCount));
    }
}

int HexBytes2Int(byte *bytes)
{
    return (bytes[3] + (bytes[2] << 8) + (bytes[1] << 16) + (bytes[0] << 24));
}

int HexStr2Int(const char *str, unsigned int len)
{
    int res = 0;
    int i;
    char c, v;
    for (i = 0; i < len; i++)
    {
        c = str[i];
        v = (c & 0xF) + ((c >> 6) | ((c >> 3) & 0x8));
        res = (res << 4) | (int)v;
    }

    return res;
}

byte HexStr2Byte(char *inStr)
{
    int b;
    b = ScanHexChar((byte)*inStr) * 16;
    b = b + ScanHexChar((byte) * (inStr + 1));
    return ((byte)b);
}

int HexStr2ByteArray(char *inStr, byte *bytePtr, int *byteLen)
{
    int i;
    char *hexChars = "0123456789ABCDEFabcdef";
    if ((strlen(inStr) % 2) != 0)
    {
        return false;
    }

    for (i = 0; i < strlen(inStr); i++)
    {
        if (strchr(hexChars, (int)inStr[i]) == NULL)
        {
            return false;
        }
    }
    for (i = 0; i < (strlen(inStr) / 2); i++)
    {
        *(bytePtr + i) = HexStr2Byte((char *)(inStr + (i * 2)));
    }
    *byteLen = i;
    return true;
}

void SetAppGlobal(int appId, int authKeyNo)
{
    openedAppGlobal.appId = appId;
    openedAppGlobal.authKey = authKeyNo;
}

void ClearAppGlobal(void)
{
    openedAppGlobal.appId = CLOSED_APP;
    openedAppGlobal.authKey = CLOSED_APP;
}
void ClearUsb(void)
{
    int readCount = 0;
    byte readBuffer[100];
    FillBytes(readBuffer, 0, sizeof(readBuffer));
    ReadBytes(CHANNEL_USB, readBuffer, readCount);
}

void StartLedTimer(void)
{
    LedTimerStart = GetSysTicks();
}

bool EndLedTimer(int ledBlinkTime)
{
    return ((GetSysTicks() > LedTimerStart + ledBlinkTime));
}
void ByteArrayEncrypt(byte *in, byte *out, int byteCount, byte *key)
{
    int i;
    CBC_ResetInitVector(CRYPTO_ENV0);
    Crypto_Init(CRYPTO_ENV0, CRYPTOMODE_AES128, key, KEY_LEN);
    for (i = 0; i < byteCount / KEY_LEN; i++)
    {
        Encrypt(CRYPTO_ENV0, (byte *)(in + (i * KEY_LEN)), (byte *)(out + (i * KEY_LEN)), KEY_LEN);
    }
}

int StrEncrypt2Byte(char *inStr, byte *out)
{
    byte buffer[PARM_BUFFER_LEN];
    int bufferLen;
    FillBytes(buffer, 0, sizeof(buffer));
    bufferLen = strlen(inStr);
    CopyBytes(buffer, (byte *)inStr, bufferLen);
    if (bufferLen % KEY_LEN > 0)
    {
        bufferLen = (bufferLen + (KEY_LEN - (bufferLen % KEY_LEN)));
    }
    ByteArrayEncrypt(buffer, out, bufferLen, (byte *)CRYPT_KEY);
    return (bufferLen);
}

void ByteArrayDecrypt(byte *in, byte *out, int inByteCount, byte *key)
{
    int i;
    CBC_ResetInitVector(CRYPTO_ENV0);
    Crypto_Init(CRYPTO_ENV0, CRYPTOMODE_AES128, key, KEY_LEN);
    for (i = 0; i < inByteCount / KEY_LEN; i++)
    {
        Decrypt(CRYPTO_ENV0, (byte *)(in + (i * KEY_LEN)), (byte *)(out + (i * KEY_LEN)), KEY_LEN);
    }
}

void NDEF_ParseRecord(byte *Buffer, NDEFRecord *record);
void NDEF_CreateRecord(byte TNF, bool MessageBegin, bool MessageEnd, bool ShortRecord, byte *Type, byte TypeLength, byte *ID, byte IDLength, byte *Payload, int PayloadLength, NDEFRecord *record);

bool ReadPassIDGoogleSmartTap(byte *PassID, int *PassIDByteCnt, int MaxPassIDByteCnt)
{
    int PassDataByteCnt;
    byte PassData[128];
    if (!ReadCardDataGoogleSmartTap(PassData, &PassDataByteCnt, sizeof(PassData)))
    {
        sprintf(mBuffer, "error reading len: %d", PassDataByteCnt);
        PrintLnDebug(mBuffer, __LINE__);
        return false;
    }
    // sprintf(mBuffer, "PassData: %s, len=%d", PassData, PassDataByteCnt);
    // PrintLnDebug(mBuffer, __LINE__);
    // DEBUG_DATALINE("PassData", PassData, PassDataByteCnt);
    NDEFRecord record;
    NDEF_ParseRecord(PassData, &record);
    byte *pPass = PassData;
    if (record.TypeLength == 3 && strncmp((const char *)&record.Buffer[record.TypeOffset], "asv", 3) == 0)
    {
        pPass = &record.Buffer[record.PayloadOffset];
        do
        {
            NDEF_ParseRecord(pPass, &record);
            if (record.TypeLength == 2 && strncmp((const char *)&record.Buffer[record.TypeOffset], "ly", 2) == 0)
            {
                pPass = &record.Buffer[record.PayloadOffset];
                do
                {
                    NDEF_ParseRecord(pPass, &record);
                    if (record.TypeLength == 1 && strncmp((const char *)&record.Buffer[record.TypeOffset], "T", 1) == 0)
                    {
                        if (record.PayloadLength == 0)
                            return false;
                        *PassIDByteCnt = MIN(MaxPassIDByteCnt, record.PayloadLength - 1);
                        memcpy(PassID, &record.Buffer[record.PayloadOffset + 1], *PassIDByteCnt);
                        return true;
                    }
                    pPass += record.Length;
                } while (!NDEF_IsMESet(record.Buffer[0]) && pPass < &PassData[PassDataByteCnt]);
            }
            pPass += record.Length;
        } while (!NDEF_IsMESet(record.Buffer[0]) && pPass < &PassData[PassDataByteCnt]);
    }
    return false;
}

int DesfireCardOpenApp(int keyAppId, int keyAuthKeyNo)
{
    const byte *authKey = NULL;
    int authKeyLen = 0;
    int authKeyType = AUTH_KEY_NO_KEY;
    int keyIndex;
    int appId;
    int authKeyNo;

    if ((openedAppGlobal.appId == keyAppId) && (openedAppGlobal.authKey == keyAuthKeyNo))
    {
        sprintf(mBuffer, "App already opened: appId=0x%x, authKeyNo=%d", keyAppId, keyAuthKeyNo);
        PrintLnDebug(mBuffer, __LINE__);
        return (true);
    }
    else
    {
        // zuerst die Applikation auswählen, anschließend schauen, ob ein Key erforderlich und falls ja mit dem Key anmelden
        openedAppGlobal.appId = CLOSED_APP;
        // If AppID is MaterAppId (>0xFFFFF0)then open ROOT_ID
        if (keyAppId == MASTER_KEY_INDEX)
        {
            appId = ROOT_ID;
            authKeyNo = ROOT_ID;
        }
        else
        {
            appId = keyAppId;
            authKeyNo = keyAuthKeyNo;
        }
        if (!DESFire_SelectApplication(CRYPTO_ENV0, appId))
        {
            sprintf(mBuffer, "App 0x%x not found.", appId);
            PrintLnDebug(mBuffer, __LINE__);
            return false;
        }

        sprintf(mBuffer, "App selected: appId=0x%x, authKeyNo=%d ", appId, authKeyNo);
        PrintLnDebug(mBuffer, __LINE__);
        for (keyIndex = 0; keyIndex < APP_KEY_COUNT; keyIndex++)
        {
            if ((appKeys[keyIndex].appId == keyAppId))
            {
                switch (authKeyNo)
                {
                case AUTH_KEY_MASTER:
                    authKey = appKeys[keyIndex].masterKey;
                    break;
                case AUTH_KEY_READ:
                    authKey = appKeys[keyIndex].readKey;
                    break;
                case AUTH_KEY_WRITE:
                    authKey = appKeys[keyIndex].writeKey;
                    break;
                case AUTH_KEY_OTHER_1:
                    authKey = appKeys[keyIndex].otherKey_1;
                    break;
                default:
                    authKey = NULL;
                    authKeyNo = AUTH_KEY_NO_KEY;
                }
                sprintf(mBuffer, "checking for Key to authenticate: appId=0x%x, keyIndex=%d, authKeyLen = %d authKeyType = %d", appId, keyIndex, appKeys[keyIndex].keyLen, appKeys[keyIndex].keyType);
                PrintLnDebug(mBuffer, __LINE__);

                if (authKeyNo != AUTH_KEY_NO_KEY)
                {
                    authKeyLen = appKeys[keyIndex].keyLen;
                    authKeyType = appKeys[keyIndex].keyType;
                    sprintf(mBuffer, "Trying to authenticate: appId=0x%x, keyIndex=%d, authKeyLen = %d authKeyType = %d", appId, keyIndex, authKeyLen, authKeyType);
                    PrintLnDebug(mBuffer, __LINE__);
                    if (DESFire_Authenticate(CRYPTO_ENV0, authKeyNo, authKey, authKeyLen, authKeyType, DESF_AUTHMODE_EV1))
                    {
                        sprintf(mBuffer, "app auth OK: appId=0x%x, authKey=%d, keyIndex=%d ", appId, authKeyNo, keyIndex);
                        PrintLnDebug(mBuffer, __LINE__);
                        SetAppGlobal(keyAppId, authKeyNo);
                        break;
                    }
                    else
                    {
                        sprintf(mBuffer, "app auth failed: appId=0x%x, authKey=%d, keyIndex=%d ", appId, authKeyNo, keyIndex);
                        PrintLnDebug(mBuffer, __LINE__);
                    }
                }
            }
        }
        if (openedAppGlobal.appId == CLOSED_APP)
        {
            sprintf(mBuffer, "app auth FAILED: appId=%d, authKey=%d", appId, authKeyNo);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }
        return (true);
    }
}

int CreateAppStdFile(int appId, int fileId, byte fileType, byte commSet, uint16_t accessRights, uint32_t fileSize, int keyOe, char *resultStr)
{
    TDESFireFileSettings fs;
    fs.FileType = fileType;
    fs.CommSet = commSet;
    fs.AccessRights = accessRights;
    fs.SpecificFileInfo.DataFileSettings.FileSize = fileSize;
    sprintf(mBuffer, "Entering CreateAppStdFile");
    PrintLnDebug(mBuffer, __LINE__);

    if (!DesfireCardOpenApp(appId, AUTH_KEY_MASTER))
    {
        sprintf(resultStr, "12,Error opening App 0x%x for creating FileNo: %d:", appId, fileId);
        return false;
    }
    if (!DESFire_CreateDataFile(CRYPTO_ENV0, fileId, &fs))
    {
        sprintf(resultStr, "11, appId=0x%x, fileId=%d, fileType=%d, accessRights=0x%x fileSize=%d, commSet=%d:Error creating File", appId, fileId, (int)fs.FileType, (int)fs.AccessRights, (int)fs.SpecificFileInfo.DataFileSettings.FileSize, (int)fs.CommSet);
        return false;
    }
    sprintf(mBuffer, "CreateAppStdFile: File create success: appId: 0x%x, fileId: %d, size: %d", appId, fileId, (int)fileSize);
    PrintLnDebug(mBuffer, __LINE__);
    return true;
}

#ifndef NO_SECURE
int DeleteApp(int masterAppId, int appId, int keyOe, char *resultStr)
{
    TDESFireMasterKeySettings mks;

    if (!DESFire_GetKeySettings(CRYPTO_ENV0, &mks))
    {
        sprintf(mBuffer, "can not read KeySettings");
        PrintLnDebug(mBuffer, __LINE__);
    }
    else
    {
        if (!mks.KeySettings.FreeCreateDelete) // no authentication necessary
        {
            if (!DesfireCardOpenApp(masterAppId, AUTH_KEY_MASTER)) // authentication with picc masterkey
            {
                sprintf(mBuffer, "Error opening App: 0x%x with Key: %d, trying with appKey", MASTER_KEY_INDEX, AUTH_KEY_MASTER);
                PrintLnDebug(mBuffer, __LINE__);
                if (!DesfireCardOpenApp(appId, AUTH_KEY_MASTER)) // authentication necessary with appId, App deleted, error ignored
                {
                    sprintf(mBuffer, "Error opening App for deleteing: 0x%x with Key: %d", appId, AUTH_KEY_MASTER);
                    PrintLnDebug(mBuffer, __LINE__);
                    sprintf(resultStr, "131,%s", mBuffer);
                    return false;
                }
                else
                {
                    sprintf(mBuffer, "Trying to delete App 0x%x ignoring error", appId);
                    PrintLnDebug(mBuffer, __LINE__);
                    DESFire_DeleteApplication(CRYPTO_ENV0, appId);
                    return (true);
                }
            }
            else
            {
            }
        }
    }
    if (!DESFire_DeleteApplication(CRYPTO_ENV0, appId))
    {
        sprintf(resultStr, "14,Error deleting App 0x%x", appId);
        return false;
    }
    return (true);
}
#endif // NO_SECURE

int ReadDesFireAppIds(byte *dataBuffer, int *dataBufferLen, int *appIdCount, char *resultStr)
{
    int appList[28];
    int appListCount, i;
    char *pStr;
    if (!DESFire_GetApplicationIDs(CRYPTO_ENV0, &appList[0], &appListCount, 28))
    {
        sprintf(mBuffer, "error reading AIDs");
        PrintLnDebug(mBuffer, __LINE__);
        sprintf(resultStr, "%s", mBuffer);
        return (false);
    }
    for (i = 0; i < appListCount; i++)
    {
        pStr = (char *)(dataBuffer + strlen((char *)dataBuffer));
        if (i == 0)
        {
            sprintf(pStr, "%x", appList[i]);
        }
        else
        {
            sprintf(pStr, ",%x", appList[i]);
        }
    }
    *appIdCount = appListCount;
    *dataBufferLen = strlen((char *)dataBuffer);
    return (true);
}

int CreateApp(int masterAppId, int appId, int keyOe, char *resultStr)
{
    TDESFireMasterKeySettings mks;
    int keyVersion[3] = {0, 0, 0};
    int keyIndex;
    int res = false;
    byte nullChar[2];

    if (!DESFire_GetKeySettings(CRYPTO_ENV0, &mks))
    {
        sprintf(mBuffer, "can not read KeySettings");
        PrintLnDebug(mBuffer, __LINE__);
    }
    else
    {
        if (!mks.KeySettings.FreeCreateDelete)
        {
            if (!DesfireCardOpenApp(masterAppId, AUTH_KEY_MASTER))
            {
                sprintf(resultStr, "13,Error opening App: 0x%x with Key: %d", MASTER_KEY_INDEX, AUTH_KEY_MASTER);
                return false;
            }
        }
    }

    switch (appId)
    {
    case DUMMY_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = true;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 1;
    case WAY2PAY_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 3;
        keyVersion[AUTH_KEY_MASTER] = 199;
        keyVersion[AUTH_KEY_READ] = 120;
        keyVersion[AUTH_KEY_WRITE] = 214;
        mks.KeyType = DESF_KEYTYPE_3DES;
        break;
    case CS_PURSE_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 3;
        keyVersion[AUTH_KEY_MASTER] = 168;
        keyVersion[AUTH_KEY_READ] = 195;
        keyVersion[AUTH_KEY_WRITE] = 12;
        mks.KeyType = DESF_KEYTYPE_3DES;
        break;
    case BWCARD_OLD_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 3;
        mks.KeyType = DESF_KEYTYPE_AES;
        keyVersion[AUTH_KEY_MASTER] = 2;
        keyVersion[AUTH_KEY_READ] = 2;
        keyVersion[AUTH_KEY_WRITE] = 2;
        break;
    case KIT_BIB_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 3;
        mks.KeyType = DESF_KEYTYPE_AES;
        keyVersion[AUTH_KEY_MASTER] = 1;
        keyVersion[AUTH_KEY_READ] = 1;
        keyVersion[AUTH_KEY_WRITE] = 1;
        break;
    case BWCARD_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 3;
        mks.KeyType = DESF_KEYTYPE_AES;
        break;
    case OSS_SIPORT_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 3;
        mks.KeyType = DESF_KEYTYPE_AES;
    case GANTNER_APP:
        mks.KeySettings.AllowChangeMasterKey = true;
        mks.KeySettings.FreeDirectoryList = true;
        mks.KeySettings.FreeCreateDelete = false;
        mks.KeySettings.ConfigurationChangeable = true;
        mks.KeySettings.ChangeKeyAccessRights = 0x0;
        mks.NumberOfKeys = 4;
        mks.KeyType = DESF_KEYTYPE_AES;

    default:
        break;
    }
    if (!DESFire_CreateApplication(CRYPTO_ENV0, appId, &mks))
    {
        sprintf(resultStr, "14,Error creating App 0x%x", appId);
        return false;
    }

    if (!DESFire_SelectApplication(CRYPTO_ENV0, appId))
    {
        sprintf(resultStr, "15,Selecting creating App 0x%x", appId);
        return false;
    }
    sprintf(mBuffer, "App create success for appId 0x%x", appId);
    PrintLnDebug(mBuffer, __LINE__);

    if ((keyIndex = GetKeyIndex(appId, keyOe)) < 0)
    {
        sprintf(resultStr, "16,No Keys for App 0x%x found.", appId);
        return false;
    }
    if (!DESFire_Authenticate(CRYPTO_ENV0, AUTH_KEY_MASTER, EMPTY_KEY, appKeys[keyIndex].keyLen, appKeys[keyIndex].keyType, DESF_AUTHMODE_EV1))
    {
        sprintf(resultStr, "17,Key change: Auth failed. App 0x%x", appId);
        return false;
    }
    if (!DESFire_ChangeKey(CRYPTO_ENV0, AUTH_KEY_WRITE, EMPTY_KEY, KEY_LEN, appKeys[keyIndex].writeKey, appKeys[keyIndex].keyLen, keyVersion[AUTH_KEY_WRITE], &mks))
    {
        sprintf(resultStr, "18,Key change: ChangeKey 2 (write) failed for App 0x%x.", appId);
        return false;
    }
    if (!DESFire_ChangeKey(CRYPTO_ENV0, AUTH_KEY_READ, EMPTY_KEY, KEY_LEN, appKeys[keyIndex].readKey, appKeys[keyIndex].keyLen, keyVersion[AUTH_KEY_READ], &mks))
    {
        sprintf(resultStr, "18,Key change: ChangeKey 1 (read) failed for App 0x%x.", appId);
        return false;
    }
    if (!DESFire_ChangeKey(CRYPTO_ENV0, AUTH_KEY_MASTER, EMPTY_KEY, KEY_LEN, appKeys[keyIndex].masterKey, appKeys[keyIndex].keyLen, keyVersion[AUTH_KEY_MASTER], &mks))
    {
        sprintf(resultStr, "18,Key change: ChangeKey 0 (master) failed for App 0x%x.", appId);
        return false;
    }
    sprintf(resultStr, "18,Key change: ChangeKey 2 (write) failed for App 0x%x.", appId);
    sprintf(mBuffer, "App 0x%x successfully created, now creating files", appId);
    PrintLnDebug(mBuffer, __LINE__);
    // Create Files for App
    switch (appId)
    {
    case WAY2PAY_APP:
        res = CreateAppStdFile(appId, 1, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1F20, 23, KIT, resultStr);
        break;
    case CS_PURSE_APP:
        res = CreateAppStdFile(appId, 1, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1F20, 300, KIT, resultStr);
        break;
    case BWCARD_OLD_APP:
        res = CreateAppStdFile(appId, 0, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_PLAIN, 0x1220, 12, KIT, resultStr);
        break;
    case KIT_BIB_APP:
        res = CreateAppStdFile(appId, 0, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_PLAIN, 0x1220, 12, KIT, resultStr);
        break;
    case BWCARD_APP:
        res = CreateAppStdFile(appId, 0, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_PLAIN, 0x1220, 12, KIT, resultStr);
        break;
    case OSS_SIPORT_APP:
        PrintLnDebug("Creating files for OSS", __LINE__);
        res = CreateAppStdFile(appId, 0, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1110, 32, KIT, resultStr);
        res = CreateAppStdFile(appId, 1, DESF_FILETYPE_BACKUPDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1110, 576, KIT, resultStr);
        res = CreateAppStdFile(appId, 2, DESF_FILETYPE_BACKUPDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1110, 128, KIT, resultStr);
        res = CreateAppStdFile(appId, 3, DESF_FILETYPE_BACKUPDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1110, 288, KIT, resultStr);
        break;
    case GANTNER_APP:
        PrintLnDebug("Creating files for Gantner App", __LINE__);
        res = CreateAppStdFile(appId, 1, DESF_FILETYPE_BACKUPDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1F20, 64, KIT, resultStr);
        res = CreateAppStdFile(appId, 2, DESF_FILETYPE_BACKUPDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1F20, 64, KIT, resultStr);
        res = DesfireCardOpenApp(GANTNER_APP, AUTH_KEY_WRITE);
        sprintf(mBuffer, "Result openApp: %s", res ? "true" : "false");
        nullChar[0] = 0x30;
        res = DESFire_WriteData(CRYPTO_ENV0, 1, (byte *)&nullChar, 0, 1, DESF_COMMSET_FULLY_ENC);
        sprintf(mBuffer, "Result WriteData: %s, file: %d, char:0x%x, ", res ? "true" : "false", 1, nullChar[0]);
        PrintLnDebug(mBuffer, __LINE__);
        res = DESFire_WriteData(CRYPTO_ENV0, 2, (byte *)&nullChar, 0, 1, DESF_COMMSET_FULLY_ENC);

        sprintf(mBuffer, "Result WriteData: %s, file: %d, char:0x%x, ", res ? "true" : "false", 2, nullChar[0]);
        PrintLnDebug(mBuffer, __LINE__);
        res = DESFire_CommitTransaction(CRYPTO_ENV0);
        sprintf(mBuffer, "Result commit: %s", res ? "true" : "false");
        PrintLnDebug(mBuffer, __LINE__);

        break;
    default:
        break;
    }
    if (!res)
    {
        sprintf(resultStr, "30,File(s) not created for App 0x%x", appId);
    }
    sprintf(resultStr, "00,App 0x%x successfully created with files.", appId);
    return true;
}

int ReadDesFireCardAppFile(int appId, int fileId, byte *dataBuffer, int *dataBufferLen, int count, int position, int keyNo, int keyOe, char *resultStr)
{
    uint32_t fileSize = 0;
    TDESFireFileSettings fileSettings;

    if (!DesfireCardOpenApp(appId, keyNo))
    {
        sprintf(resultStr, "102:Error opening App 0x%x for reading FileNo: %d:", appId, fileId);
        return false;
    }

    // Filsize ermitteln
    if (!DESFire_GetFileSettings(CRYPTO_ENV0, fileId, &fileSettings))
    {
        sprintf(resultStr, "103,Error in reading FileSettings");
        return (false);
    }
    else
    {
        if ((fileSettings.FileType != DESF_FILETYPE_STDDATAFILE) && (fileSettings.FileType != DESF_FILETYPE_BACKUPDATAFILE))
        {
            sprintf(resultStr, "32,FileType not supported: FileType = %d", fileSettings.FileType);
            return (false);
        }

        if (!(fileSize = fileSettings.SpecificFileInfo.DataFileSettings.FileSize) > *dataBufferLen)
        {
            sprintf(resultStr, "33,FileSize: %u > dataBufferLen %d", (int)fileSize, *dataBufferLen);
            PrintLnDebug(resultStr, __LINE__);
            return (false);
        }
        if ((int)fileSize < count + position)
        {
            sprintf(resultStr, "33,FileSize: %u < requested data=%d", (int)fileSize, count + position);
            PrintLnDebug(resultStr, __LINE__);
            return (false);
        }
    }
    if (count == 0)
    {
        count = (int)fileSize;
    }
    // Daten lesen
    sprintf(mBuffer, "ReadData:appId=0x%x, fileId=%d, position=%d, count=%d, comSet=%d", appId, fileId, position, count, fileSettings.CommSet);
    PrintLnDebug(mBuffer, __LINE__);

    if (!DESFire_ReadData(CRYPTO_ENV0, fileId, dataBuffer, position, count, fileSettings.CommSet))
    {

        sprintf(resultStr, "34,ReadDesFireCardAppFile read failed");
        PrintLnDebug(resultStr, __LINE__);
        return (false);
    }
    *dataBufferLen = count;
    ConvertBinaryToString(dataBuffer, 0, *dataBufferLen * 8, outBufferStr, 16, *dataBufferLen * 2, *dataBufferLen * 2);
    sprintf(mBuffer, "ReadDesFireCardAppFile End with dataBufferLen: %d, dataBuffer: %s", *dataBufferLen, outBufferStr);
    PrintDebug(mBuffer);
    sprintf(resultStr, "0,%s", outBufferStr);
    return true;
}

int WriteDesFireCardAppFile(int appId, int fileId, byte *fileData, int fileDataLen, int fileOffset, int keyNo, int keyOe, char *resultStr)
{
    int fileSize;
    byte fileBuffer[500];

    TDESFireFileSettings fileSettings;

    if (!DesfireCardOpenApp(appId, keyNo))
    {
        sprintf(resultStr, "102:Error opening App 0x%x for writing FileNo: %d:", appId, fileId);
        return false;
    }

    if (!DESFire_GetFileSettings(CRYPTO_ENV0, fileId, &fileSettings))
    {
        sprintf(resultStr, "40,Error WriteDesFireCardAppFile Read FileSetting from fileId %d from App 0x%x failed", fileId, appId);
        return false;
    }

    if ((fileSize = (int)fileSettings.SpecificFileInfo.DataFileSettings.FileSize) < fileDataLen)
    {
        sprintf(resultStr, "42,Erro on WriteDesFireCardApp:FileDataLen: %d > FileSize: %d ", fileDataLen, fileSize);
        return false;
    }

    FillBytes(fileBuffer, 0, sizeof(fileBuffer));
    CopyBytes(fileBuffer, fileData, fileDataLen);
    ConvertBinaryToString(fileBuffer, 0, 8 * fileDataLen, outBufferStr, 16, 2 * fileDataLen, 2 * fileDataLen);
    sprintf(mBuffer, "appId=0x%x, fileId=%d fileOffset=%d, fileDataLen=%d, comSet=%d, fileBuffer=%s", appId, fileId, fileOffset, fileDataLen, fileSettings.CommSet, outBufferStr);
    PrintLnDebug(mBuffer, __LINE__);

    if (!DESFire_WriteData(CRYPTO_ENV0, fileId, fileBuffer, fileOffset, fileDataLen, fileSettings.CommSet))
    {
        sprintf(resultStr, "43,Error WriteDesFireCardAppFile Write File fileId %d from App 0x%x failed", fileId, appId);
        return false;
    }
    if (fileSettings.FileType == DESF_FILETYPE_BACKUPDATAFILE)
    {
        if (!DESFire_CommitTransaction(CRYPTO_ENV0))
        {
            sprintf(resultStr, "44,Error: DESFire_CommitTransaction failed");
            PrintLnDebug(resultStr, __LINE__);
            return false;
        }
    }
    sprintf(mBuffer, "WriteDesFireCardAppFile Write Data Success: fileId: %d, appId: 0x%x", fileId, appId);
    PrintLnDebug(mBuffer, __LINE__);
    return true;
}

int CheckForApp(int appId)
{
    if (DESFire_SelectApplication(CRYPTO_ENV0, appId))
    {
        return true;
    }
    else
    {
        return false;
    }
}

int CheckForAppFile(int appId, int fileId)
{
    byte fileIdsList[32];
    int fileIdFound = false;
    int fileIdCount = -1;
    int i;

    if (!DESFire_GetFileIDs(CRYPTO_ENV0, fileIdsList, &fileIdCount, sizeof(fileIdsList)))
    {
        sprintf(mBuffer, "CheckForAppFile, App: 0x%x - Error Reading  fileIds", appId);
        PrintLnDebug(mBuffer, __LINE__);
        return (false);
    }
    for (i = 0; i < fileIdCount; i++)
    {
        if (fileId == (int)(fileIdsList[i]))
            fileIdFound = true;
    }
    if (!fileIdFound)
    {
        sprintf(mBuffer, "CheckForAppFile, App: 0x%x fileId: %d, file not found", appId, fileId);
        PrintLnDebug(mBuffer, __LINE__);
        return (false);
    }
    else
    {
        sprintf(mBuffer, "CheckForAppFile: Number of fileIds: %d for App: 0x%x", fileIdCount, appId);
        PrintLnDebug(mBuffer, __LINE__);
    }
    PrintLnDebug("CheckForApp ended", __LINE__);

    return (fileIdFound);
}

void ReadDeviceUid(char *result)
{
    byte deviceUid[DEVICE_UID_LEN];
    GetDeviceUID(deviceUid);
    ConvertBinaryToString(deviceUid, 0, DEVICE_UID_LEN * 8, result, 16, DEVICE_UID_LEN * 2, 50);
}

int ReadDesFireCardUID(int appId, char *resultBuffer, int *resultBufferLen, byte* uid)
{
    int uidLen = 0;
    if (appId > -1)
    {
        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultBuffer, "201,Error opening App: 0x%x with Key: %d", BWCARD_OLD_APP, AUTH_KEY_READ);
            return (false);
        }
    }
    if (DESFire_GetUID(CRYPTO_ENV0, uid, &uidLen, sizeof(uid)))
    {
        sprintf(mBuffer, "uid: %02X%02X%02X%02X%02X%02X%02X", (unsigned int)uid[0], (unsigned int)uid[1], (unsigned int)uid[2], (unsigned int)uid[3], (unsigned int)uid[4], (unsigned int)uid[5], (unsigned int)uid[6]);
        PrintLnDebug(mBuffer, __LINE__);

        *resultBufferLen = ConvertBinaryToString(uid, 0, UID_SIZE * 8, (char *)resultBuffer, 16, UID_SIZE * 2, 20);
    }
    else
    {
        PrintError("UID read failed.", resultBuffer);
        return (false);
    }
    return (true);
}

int ReadAction(int action, char *parmStr, byte *parmByteBuffer)
{
    int readCount;
    byte readBuffer[PARM_BUFFER_LEN];
    byte *bPtr;
    char *pStr;
    bool eolRead = false;


    FillBytes(readBuffer, 0, sizeof(readBuffer));
    bPtr = readBuffer;
    do
    {
        readCount = GetByteCount(CHANNEL_USB, DIR_IN);
        ReadBytes(CHANNEL_USB, bPtr, readCount);
        bPtr = bPtr + readCount;
        if ((pStr = strchr((char *)readBuffer, 0x0D)) != NULL)
        {
            readBuffer[(pStr - (char *)readBuffer)] = 0x0;
            eolRead = true;
        }
    } while (!eolRead);

    if (strlen((char *)readBuffer) > 0)
    {

        if ((pStr = strchr((char *)readBuffer, ',')) != NULL)
        {
            strcpy(parmStr, pStr + 1);
            *pStr = 0x0;
        }

        action = Byte2Int(readBuffer, strlen((char *)readBuffer));
        sprintf(mBuffer, "ReadAction -> action: %d, parmStr: \"%s\"", action, parmStr);
        PrintLnDebug(mBuffer, __LINE__);
    }
    return (action);
}

void ChangeSettings(void)
{
    TDESFireMasterKeySettings mks;

    if (!DESFire_GetKeySettings(CRYPTO_ENV0, &mks))
    {
        if (DesfireCardOpenApp(MASTER_KEY_INDEX, 0))
        {
            sprintf(mBuffer, "Card authenticated with key");
            PrintLnDebug(mBuffer, __LINE__);
            if (DESFire_GetKeySettings(CRYPTO_ENV0, &mks))
            {
                mks.KeySettings.FreeDirectoryList = 1;
                if (DESFire_ChangeKeySettings(CRYPTO_ENV0, &mks))
                {
                    PrintLnDebug("Key Setting changed to FreeDirectoryList", __LINE__);
                }
                else
                {
                    PrintLnDebug("Cannot change Key Setting to FreeDirectoryList", __LINE__);
                }
            }
            else
            {
                PrintLnDebug("Cannot read KeySettings for ChangeKeySettings", __LINE__);
            }
        }
    }

    else
    {
        PrintLnDebug("Card Setting read allowed", __LINE__);
    }
    if (DesfireCardOpenApp(BWCARD_OLD_APP, AUTH_KEY_READ))
    {
        if (!DESFire_GetKeySettings(CRYPTO_ENV0, &mks))
        {
            PrintLnDebug("Cannot read bwCard App Settings ", __LINE__);
            if (DesfireCardOpenApp(BWCARD_OLD_APP, AUTH_KEY_MASTER))
            {
                mks.KeySettings.FreeDirectoryList = 1;
                if (DESFire_ChangeKeySettings(CRYPTO_ENV0, &mks))
                {
                    PrintLnDebug("Key for bwCard App Setting changed to FreeDirectoryList", __LINE__);
                }
                else
                {
                    PrintLnDebug("Cannot for bwCard App change Key Setting to FreeDirectoryList", __LINE__);
                }
            }
        }
        else
        {
            sprintf(mBuffer, "bwCard App Settings read allowed.");
            PrintLnDebug(mBuffer, __LINE__);
        }
    }
}

int mergeDoorInfo(byte *resultBuffer, int *resultBufferLen, byte *addBuffer, int addBufferLen, byte *cardBuffer, int cardBufferLen, byte *deleteBuffer, int deleteBufferLen)
{
    int i;
    int j;
    int cmp;

    PrintLnDebug("mergeDoorInfo started", __LINE__);
    ConvertBinaryToString(addBuffer, 0, addBufferLen * 8, outBufferStr, 16, addBufferLen * 2, sizeof(outBufferStr));
    sprintf(mBuffer, "Start:addBuffer=%s, addBuffer=%d", outBufferStr, addBufferLen);
    PrintLnDebug(mBuffer, __LINE__);
    ConvertBinaryToString(cardBuffer, 0, cardBufferLen * 8, outBufferStr, 16, cardBufferLen * 2, sizeof(outBufferStr));
    sprintf(mBuffer, "Start:cardBuffer=%s, cardBufferLen=%d", outBufferStr, cardBufferLen);
    PrintLnDebug(mBuffer, __LINE__);
    ConvertBinaryToString(deleteBuffer, 0, deleteBufferLen * 8, outBufferStr, 16, deleteBufferLen * 2, sizeof(outBufferStr));
    sprintf(mBuffer, "Start:deleteBuffer=%s, deleteBufferLen=%d", outBufferStr, deleteBufferLen);
    PrintLnDebug(mBuffer, __LINE__);
    memset(resultBuffer, 0, OSS_DATA_DOORINFO_COUNT * ossDoorInfoLen);
    *resultBufferLen = 0;

    for (i = 0; i < addBufferLen / 2; i++)
    {
        memcpy((byte *)(resultBuffer + *resultBufferLen), (byte *)(addBuffer + i * 2), 2);
        *resultBufferLen += ossDoorInfoLen;
    }

    for (i = 0; i < cardBufferLen / ossDoorInfoLen; i++)
    {
        cmp = 1;
        for (j = 0; j < deleteBufferLen / 2; j++)
        {
            cmp = cmp * memcmp((byte *)(cardBuffer + i * ossDoorInfoLen), (byte *)(deleteBuffer + j * 2), 2);
        }
        if (cmp != 0)
        {
            memcpy((byte *)(resultBuffer + *resultBufferLen), (byte *)(cardBuffer + i * ossDoorInfoLen), ossDoorInfoLen);
            *resultBufferLen += ossDoorInfoLen;
        }
    }

    ConvertBinaryToString((byte *)resultBuffer, 0, *resultBufferLen * 8, outBufferStr, 16, *resultBufferLen * 2, sizeof(outBufferStr));
    sprintf(mBuffer, "resultBuffer End: %s, bufferLen: %d", outBufferStr, *resultBufferLen);
    PrintLnDebug(mBuffer, __LINE__);
    PrintLnDebug("mergeDoorInfo ended", __LINE__);
    return (true);
}

int CheckForDoorInfo(OssDataDoorInfo *doorInfoBuffer, int doorInfoIndex, OssDataDoorInfo *doorInfo)
{
    int i;
    for (i = 0; i < doorInfoIndex; i++)
    {
        if (((doorInfoBuffer + i)->DoorId[0] == doorInfo->DoorId[0]) && ((doorInfoBuffer + i)->DoorId[1] == doorInfo->DoorId[1]))
            return (true);
    }
    return (false);
}

// *******************************************************************
// ****** Google Pay Smart Tap (ID, any length) **********************
// *******************************************************************

bool ReadGoogleSmartTap(char *CardString, int MaxCardStringLen)
{
    // ------ STEP 1: Test and read data from transponder ------------

    byte CardData[256];
    int CardDataBitCnt;
    int CardDataByteCnt;
    if (!ReadPassIDGoogleSmartTap(CardData, &CardDataByteCnt, sizeof(CardData)))
        return false;
    CardDataBitCnt = CardDataByteCnt * 8;
    if (CardDataBitCnt == 0)
        return false;

    // ------ STEP 2: Do bit manipulation ----------------------------

    // (No bit manipulation specified)

    // ------ STEP 3: Format output data -----------------------------
    int RemainingDigits, MinFieldDigits, MaxFieldDigits, FieldBitCnt;
    int DigitCnt, FillByteCount;
    char *WritePos = CardString;
    *WritePos = 0;
    // ------ Field 1 (ASCII) ----------------------------------------
    RemainingDigits = MaxCardStringLen - strlen(CardString);
    // Output all digits of the field
    MaxFieldDigits = RemainingDigits;
    MinFieldDigits = 0;
    FieldBitCnt = CardDataBitCnt;
    DigitCnt = MIN((FieldBitCnt + 7) / 8, MaxFieldDigits);
    FillByteCount = MAX(MinFieldDigits - DigitCnt, 0);
    FillBytes((byte *)WritePos, '0', FillByteCount);
    WritePos += FillByteCount;
    CopyBits((byte *)WritePos, 0, CardData, 0, DigitCnt * 8);
    WritePos += DigitCnt;
    *WritePos = 0;
    // ---------------------------------------------------------------

    return true;
}

int DoDesFireCardAction(int action, char *parmStr, byte *uid, char *resultStr)
{
    int byteBufferLen;
    byte byteBuffer[250];
    int byteBuffer2Len;
    byte byteBuffer2[250];
    char bwCardNumber[200];
    char *parmStrPtr;
    char bwCardNumberEnc[200];
    byte appFileBuffer[PARM_BUFFER_LEN];
    int appFileBufferLen = sizeof(appFileBuffer);
    byte appFileWriteBuffer[PARM_BUFFER_LEN];
    int appFileWriteBufferLen = sizeof(appFileWriteBuffer);
    int parmDataLen;

    char cardUid[20];
    char deviceUid[50];
    OssInfoFileData ossInfoFileData[32];
    OssInfoFileData *pOssInfoFileData;
    OssDataFileData *pOssDataReadFileData;
    OssDataFileData *pOssDataWriteFileData;
    OssDataDoorInfo ossDataDoorInfoBuffer[OSS_DATA_DOORINFO_COUNT];
    int ossDataDoorInfoIndex = 0;
    byte *pOssPtr;

    int ossDoorId = 0;
    int appId = 0;
    int fileId;
    int position;
    int count;
    int actSeconds = 0;
    int cardSeconds = 0;
    char cardKeyStr[50];

#ifdef SERIAL_ACTION
    if ((action != SERIAL_ACTION) && (action != ACTION_WAIT_FOR_CARD))
    {
        sprintf(resultStr, "01,Error: Unsupported action: %d, action supported: %d", action, SERIAL_ACTION);
        return (false);
    }
#endif
    memset(resultStr, 0, MBUFFER_LEN);

    switch (action)
    {
    case ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID_CARDUID:
    case ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED_DEVICEUID:
    case ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID:
    case ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED:
    case ACTION_BWCARD_OLD_READ_NUMBER:
    case ACTION_BWCARD_OLD_READ_NUMBER_UID:
    case ACTION_BWCARD_OLD_READ_NUMBER_BEEP:
    case ACTION_UID_DEC_MSB:
    case ACTION_UID_HEX_DEC_LSB:
    case ACTION_BWCARD_OLD_READ_NUMBER_FILE_1:
    case ACTION_BWCARD_OLD_WRITE_FILE_1:
    case ACTION_BWCARD_OLD_UID_READ_NUMBER_FILE_1:
    case ACTION_BWCARD_OLD_3G_CHECK:
    case ACTION_BWCARD_OLD_3G_CHECK_ENCRYPTED:
    case ACTION_BWCARD_OLD_CHECK_FILE_1:
        appId = BWCARD_OLD_APP;
        ChangeSettings();
    case ACTION_BWCARD_READ_NUMBER_DEVICEUID_CARDUID:
    case ACTION_BWCARD_READ_NUMBER_ENCRYPTED_DEVICEUID:
    case ACTION_BWCARD_READ_NUMBER_DEVICEUID:
    case ACTION_BWCARD_READ_NUMBER_ENCRYPTED:
    case ACTION_BWCARD_READ_NUMBER:
    case ACTION_GOOGLESMARTTAP_READ_WAIT:
    case ACTION_GOOGLESMARTTAP_READ:
    case ACTION_GOOGLESMARTTAP_BWCARD_READ_WAIT:
    case ACTION_GOOGLESMARTTAP_BWCARD_READ:

        if (appId == 0)
        {
            appId = BWCARD_APP;
        }

        byteBufferLen = sizeof(bwCardNumber);
        FillBytes((byte *)bwCardNumber, 0, byteBufferLen);
        byteBufferLen = sizeof(cardUid);
        FillBytes((byte *)cardUid, 0, sizeof(byteBufferLen));
        // App oeffnen
        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            appId = MASTER_KEY_INDEX;
            // sprintf(resultStr, "103,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            // return (false);
        }
        // UID lesen und in cardUid speichern
        if (ReadDesFireCardUID(appId, cardUid, &byteBufferLen, uid))
        {
            sprintf(mBuffer, "ReadDesFireCardUID: DESFire appId: 0x%x, resultbuffer: %s", appId, cardUid);
            PrintLnDebug(mBuffer, __LINE__);
        }
        else
        {
            byteBufferLen = ConvertBinaryToString(uid, 0, UID_SIZE * 8, (char *)cardUid, 16, UID_SIZE * 2, 20);
            appId = MASTER_KEY_INDEX;
        }
        if (appId != MASTER_KEY_INDEX)
        {
            // cardNumber lesen und in bwCardNumber speichern
            if (!ReadDesFireCardAppFile(appId, BWCARD_DESFIRE_APP_FILEID, (byte *)bwCardNumber, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
            {
                sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "%s", mBuffer);
                return (false);
            }

            sprintf(mBuffer, "cardNumber read: bwCardNumber=%s, bufferLen=%d", bwCardNumber, byteBufferLen);
            PrintLnDebug(mBuffer, __LINE__);

            // File 1 lesen, wenn er existiert und in outBufferStr speichern
            if (appId == BWCARD_OLD_APP)
            {
                if (CheckForAppFile(appId, 1))
                {
                    if (!ReadDesFireCardAppFile(appId, 1, byteBuffer, &byteBufferLen, 0x40, 0, AUTH_KEY_READ, KIT, resultStr))
                    {
                        sprintf(mBuffer, "291,Error reading  file1 of bwcard");
                        PrintLnDebug(mBuffer, __LINE__);
                        sprintf(resultStr, "%s", mBuffer);
                        return (false);
                    }
                    sprintf(mBuffer, "File read:%s", (char *)byteBuffer);
                    PrintLnDebug(mBuffer, __LINE__);
                    sprintf(outBufferStr, "%s", (char *)byteBuffer);
                }
                else
                {
                    sprintf(mBuffer, "No Appfile 1 for 0x%x found", appId);
                    FillBytes((byte *)outBufferStr, 0, sizeof(outBufferStr));
                    PrintLnDebug(mBuffer, __LINE__);
                }
            }
            // Kartennummer verschlüsslen und in bwCardEnc speichern

            byteBufferLen = sizeof(bwCardNumberEnc);
            FillBytes((byte *)bwCardNumberEnc, 0, byteBufferLen);
            ByteArrayEncrypt((byte *)bwCardNumber, byteBuffer, byteBufferLen, (byte *)CRYPT_KEY);
            ConvertBinaryToString(byteBuffer, 0, 128, bwCardNumberEnc, 16, 32, byteBufferLen);
            // Device UID lesen
            ReadDeviceUid(deviceUid);
            sprintf(mBuffer, "bwCardNumber: %s, bwCardNumberEnc: %s, cardUid: %s, deviceUid:%s", bwCardNumber, bwCardNumberEnc, cardUid, deviceUid);
            PrintLnDebug(mBuffer, __LINE__);

            // Ergebnis zusammenbauen
        }
        memset(resultStr, 0, sizeof(resultStr));
        switch (action)
        {
        case ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID_CARDUID:
        case ACTION_BWCARD_READ_NUMBER_DEVICEUID_CARDUID:
            sprintf(resultStr, "%s;%s;%s", bwCardNumber, cardUid, deviceUid);
            break;
        case ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED_DEVICEUID:
        case ACTION_BWCARD_READ_NUMBER_ENCRYPTED_DEVICEUID:
            sprintf(resultStr, "%s;%s", bwCardNumberEnc, deviceUid);
            break;
        case ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED:
        case ACTION_BWCARD_READ_NUMBER_ENCRYPTED:
            sprintf(resultStr, "%s", bwCardNumberEnc);
            break;
        case ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID:
        case ACTION_BWCARD_READ_NUMBER_DEVICEUID:
            sprintf(resultStr, "%s;%s", bwCardNumber, deviceUid);
            break;
        case ACTION_BWCARD_OLD_READ_NUMBER:
        case ACTION_BWCARD_READ_NUMBER:
        case ACTION_BWCARD_OLD_READ_NUMBER_BEEP:
            sprintf(resultStr, "%s", bwCardNumber);
            break;
        case ACTION_UID_DEC_MSB:
            {
                // convert UID to decimal and cut off the first 4 digits
                ConvertBinaryToString(uid, 0, UID_SIZE * 8, outBufferStr, 10, UID_SIZE * 2, UID_SIZE * 4);
                sprintf(resultStr, "%s", outBufferStr);
            }
            break;
        case ACTION_UID_HEX_DEC_LSB:
            {
                // convert UID to decimal and cut off the first 4 digits
                // memset(byteBuffer, 0, sizeof(byteBuffer));
                SwapBytes(uid, UID_SIZE);
                ConvertBinaryToString(uid, 0, UID_SIZE * 8, outBufferStr, 10, UID_SIZE * 2, UID_SIZE * 4);
                PrintLnDebug(outBufferStr, __LINE__);
                sprintf(resultStr, "%s;%s", cardUid, outBufferStr);
            }
            break;
        case ACTION_BWCARD_OLD_READ_NUMBER_UID:
            sprintf(resultStr, "%s;%s", bwCardNumber, cardUid);
            break;
        case ACTION_BWCARD_OLD_READ_NUMBER_FILE_1:
            sprintf(resultStr, "%s;%s", bwCardNumber, outBufferStr);
            break;
        case ACTION_BWCARD_OLD_UID_READ_NUMBER_FILE_1:
            sprintf(resultStr, "%s;%s", cardUid, outBufferStr);
            break;
        case ACTION_BWCARD_OLD_WRITE_FILE_1:
            // size of time in seconds on card (int)
            fileId = 1;
            // Hex representation = 2* count
            appFileBufferLen = MIN(strlen(parmStr), 0x40);
            CopyBytes(appFileBuffer, (byte *)parmStr, appFileBufferLen);
            sprintf(mBuffer, "data write appId=0x%x, fileId=%d, appFileBuffer= %s appFileBufferLen=%d, parmStr= %s", appId, fileId, (char *)appFileBuffer, appFileBufferLen, parmStr);
            PrintLnDebug(mBuffer, __LINE__);

            if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
            {
                sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
                return (false);
            }

            if (CheckForAppFile(appId, fileId))
            // delete old data
            {
                byteBufferLen = 0x40;
                FillBytes(byteBuffer, 0, byteBufferLen);
                if (!WriteDesFireCardAppFile(appId, fileId, byteBuffer, byteBufferLen, 0, AUTH_KEY_WRITE, KIT, resultStr))
                {
                    sprintf(mBuffer, "Error WriteDesFireCardAppFile for clear: %s", resultStr);
                    PrintLnDebug(mBuffer, __LINE__);
                    sprintf(resultStr, "753, %s", mBuffer);
                    return (false);
                }
            }
            else
            {
                // force reopen App
                if (!CreateAppStdFile(appId, fileId, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_PLAIN, 0x1220, 0x40, KIT, resultStr))
                {
                    sprintf(mBuffer, "751, AppId: 0x%x, fileId: %d - File not created: %s", appId, 1, resultStr);
                    PrintLnDebug(mBuffer, __LINE__);
                    return (false);
                }
            }
            if (!WriteDesFireCardAppFile(appId, fileId, appFileBuffer, appFileBufferLen, 0, AUTH_KEY_WRITE, KIT, resultStr))
            {
                sprintf(mBuffer, "Error WriteDesFireCardAppFile: %s", resultStr);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "753, %s", mBuffer);
                return (false);
            }
            sprintf(mBuffer, "App=0x%x, fileId=%d, Data written", appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "0,%s", mBuffer);
            break;
        case ACTION_BWCARD_OLD_3G_CHECK:
            if (strlen(outBufferStr) > 0)
            {
                actSeconds = Dec2Int((byte *)parmStr, strlen(parmStr));
                if (actSeconds == 0)
                {
                    sprintf(resultStr, "171,%s;False", bwCardNumber);
                }
                else
                {
                    cardSeconds = HexStr2Int(outBufferStr + 8, 8);
                    sprintf(mBuffer, "3G seconds: parmStr=%s, actSeconds=%d, regSeconds=%d", parmStr, actSeconds, cardSeconds);
                    PrintLnDebug(mBuffer, __LINE__);
                    (actSeconds <= cardSeconds) ? sprintf(resultStr, "00,%s;True", bwCardNumber) : sprintf(resultStr, "00,%s;False", bwCardNumber);
                }
            }
            else
            {
                sprintf(resultStr, "00,%s;False", bwCardNumber);
            }
            break;
        case ACTION_BWCARD_OLD_3G_CHECK_ENCRYPTED:
            if (strlen(outBufferStr) > 0)
            {
                actSeconds = Dec2Int((byte *)parmStr, strlen(parmStr));
                if (actSeconds == 0)
                {
                    sprintf(resultStr, "181%s;False", bwCardNumber);
                }
                else
                {
                    cardSeconds = HexStr2Int(outBufferStr + 8, 8);
                    sprintf(mBuffer, "3G seconds: parmStr=%s, actSeconds=%d, regSeconds=%d", parmStr, actSeconds, cardSeconds);
                    PrintLnDebug(mBuffer, __LINE__);
                    (actSeconds <= cardSeconds) ? sprintf(resultStr, "00,%s;True", bwCardNumberEnc) : sprintf(resultStr, "00,%s;False", bwCardNumberEnc);
                }
            }
            else
            {
                sprintf(resultStr, "00,%s;False", bwCardNumberEnc);
            }
            break;
        case ACTION_BWCARD_OLD_CHECK_FILE_1:
            if (strlen(outBufferStr) > 0)
            {
                actSeconds = Dec2Int((byte *)parmStr, strlen(parmStr));
                if (actSeconds == 0)
                {
                    sprintf(resultStr, "False");
                }
                else
                {
                    cardSeconds = HexStr2Int(outBufferStr + 8, 8);
                    sprintf(mBuffer, "3G seconds: parmStr=%s, actSeconds=%d, regSeconds=%d", parmStr, actSeconds, cardSeconds);
                    PrintLnDebug(mBuffer, __LINE__);
                    (actSeconds <= cardSeconds) ? sprintf(resultStr, "True") : sprintf(resultStr, "False");
                }
            }
            else
            {
                sprintf(resultStr, "False");
            }
            break;
        case ACTION_GOOGLESMARTTAP_BWCARD_READ:
        case ACTION_GOOGLESMARTTAP_BWCARD_READ_WAIT:
            if (strlen(bwCardNumber) > 0)
            {
                sprintf(resultStr, "%s", bwCardNumber);
                break;
            }
        case ACTION_GOOGLESMARTTAP_READ:
        case ACTION_GOOGLESMARTTAP_READ_WAIT:
        {
            sprintf(mBuffer, "ACTION_GOOGLESMARTTAP_READ entered with resultStr: %s", resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            ReadGoogleSmartTap(resultStr, MBUFFER_LEN - 1);
            sprintf(mBuffer, "ACTION_GOOGLESMARTTAP_READ resultStr: %s", resultStr);
            PrintLnDebug(mBuffer, __LINE__);

            byte ID[MAXIDBYTES];
            int IDBitCnt;
            int TagType;

            SearchTag(&TagType, &IDBitCnt, ID, sizeof(ID));
        }
        break;
        }
        break;
    case ACTION_KIT_CARD_READ_NUMBER:
    case ACTION_KIT_CARD_READ_NUMBER_ENCRYPTED:
        appId = KIT_CARD_APP;
        fileId = 0;
        byteBufferLen = sizeof(bwCardNumber);
        FillBytes((byte *)bwCardNumber, 0, byteBufferLen);
        byteBufferLen = sizeof(cardUid);
        FillBytes((byte *)cardUid, 0, sizeof(byteBufferLen));
        // App oeffnen
        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "103,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        // cardNumber lesen und in bwCardNumber speichern
        if (!ReadDesFireCardAppFile(appId, BWCARD_DESFIRE_APP_FILEID, (byte *)bwCardNumber, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }

        sprintf(mBuffer, "cardNumber read: bwCardNumber=%s, bufferLen=%d", bwCardNumber, byteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        byteBufferLen = sizeof(bwCardNumberEnc);
        FillBytes((byte *)bwCardNumberEnc, 0, byteBufferLen);
        ByteArrayEncrypt((byte *)bwCardNumber, byteBuffer, byteBufferLen, (byte *)CRYPT_KEY);
        ConvertBinaryToString(byteBuffer, 0, 128, bwCardNumberEnc, 16, 32, byteBufferLen);
        if (action == ACTION_KIT_CARD_READ_NUMBER)
        {
            sprintf(resultStr, "%s", bwCardNumber);
        }
        else
        {
            byteBufferLen = sizeof(bwCardNumberEnc);
            FillBytes((byte *)bwCardNumberEnc, 0, byteBufferLen);
            ByteArrayEncrypt((byte *)bwCardNumber, byteBuffer, byteBufferLen, (byte *)CRYPT_KEY);
            ConvertBinaryToString(byteBuffer, 0, 128, bwCardNumberEnc, 16, 32, byteBufferLen);
            sprintf(resultStr, "%s", bwCardNumberEnc);
        }
        break;

    case ACTION_BWCARD_READ_ALL:
        appId = BWCARD_APP;
        // App oeffnen
        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "103,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        FillBytes((byte *)bwCardNumber, 0, sizeof(bwCardNumber));
        // cardNumber lesen und in bwCardNumber speichern
        byteBufferLen = sizeof(byteBuffer);
        FillBytes((byte *)byteBuffer, 0, byteBufferLen);
        if (!ReadDesFireCardAppFile(appId, 0, (byte *)byteBuffer, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        sprintf(mBuffer, "File0 read: bwCardNumber=%s, bufferLen=%d", (char *)byteBuffer, byteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        sprintf(bwCardNumber, "%s;", (char *)byteBuffer);
        sprintf(mBuffer, "bwCardAll File 0: %s", bwCardNumber);
        PrintLnDebug(mBuffer, __LINE__);

        // schacHome lesen und an bwCardNumber anhaengen
        byteBufferLen = sizeof(byteBuffer);
        FillBytes((byte *)byteBuffer, 0, byteBufferLen);
        if (!ReadDesFireCardAppFile(appId, 1, (byte *)byteBuffer, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        sprintf(mBuffer, "File1 read: schacHome=%s, bufferLen=%d", (char *)byteBuffer, byteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        strcat(bwCardNumber, (char *)byteBuffer);
        sprintf(mBuffer, "bwCardAll File 1: %s", bwCardNumber);
        PrintLnDebug(mBuffer, __LINE__);

        // UID lesen und an bwCardNumber anhaengen
        byteBufferLen = sizeof(byteBuffer);
        FillBytes((byte *)byteBuffer, 0, byteBufferLen);
        if (!ReadDesFireCardAppFile(appId, 2, (byte *)byteBuffer, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        ConvertBinaryToString(byteBuffer, 0, 8 * 10, outBufferStr, 16, 2 * 10, 2 * 10);
        sprintf(mBuffer, "File2 read: UID=%s, bufferLen=%d", (char *)byteBuffer, byteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        strcat(bwCardNumber, ";");
        strcat(bwCardNumber, outBufferStr);
        sprintf(mBuffer, "bwCardAll File 2: %s", bwCardNumber);
        PrintLnDebug(mBuffer, __LINE__);

        // ESCN lesen und an bwCardNumber anhaengen
        byteBufferLen = sizeof(byteBuffer);
        FillBytes((byte *)byteBuffer, 0, byteBufferLen);
        if (!ReadDesFireCardAppFile(appId, 3, (byte *)byteBuffer, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        sprintf(mBuffer, "File3 read: escn=%s, bufferLen=%d", (char *)byteBuffer, byteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        strcat(bwCardNumber, ";");
        strcat(bwCardNumber, (char *)byteBuffer);
        sprintf(mBuffer, "bwCardAll File 3: %s", bwCardNumber);
        PrintLnDebug(mBuffer, __LINE__);

        sprintf(resultStr, "%s", bwCardNumber);
        break;
    // case ACTION_DELETE_APP:
    //     appId = HexStr2Int(parmStr, strlen(parmStr));
    //     if (!CheckForApp(appId))
    //     {
    //         sprintf(resultStr, "101,App 0x%x not found, nothing to delete", appId);
    //         return false;
    //     }
    //     if (!DesfireCardOpenApp(MASTER_KEY_INDEX, AUTH_KEY_MASTER))
    //     {
    //         sprintf(resultStr, "102,Error opening App: 0x%x with Key: %d", MASTER_KEY_INDEX, AUTH_KEY_MASTER);
    //         return false;
    //     }
    //     if (!DESFire_DeleteApplication(CRYPTO_ENV0, appId))
    //     {
    //         sprintf(resultStr, "103,Error Deleting App: 0x%x", appId);
    //         return (false);
    //     }

    //     sprintf(resultStr, "0,App 0x%x deleted", appId);

    //     break;
    case ACTION_CREATE_OSS_APP:
        FillBytes((byte *)ossInfoFileData, 0, sizeof(*ossInfoFileData));
        if (CheckForApp(OSS_SIPORT_APP))
        {
            sprintf(mBuffer, "OSS App already on Card, checking for update");
            PrintLnDebug(mBuffer, __LINE__);
            FillBytes(appFileBuffer, 0, sizeof(appFileBuffer));
            if (!ReadDesFireCardAppFile(OSS_SIPORT_APP, OSS_SIPORT_DESFIRE_APP_INFO_FILEID, appFileBuffer, &appFileBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
            {
                sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", OSS_SIPORT_APP, OSS_SIPORT_DESFIRE_APP_INFO_FILEID, resultStr);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "%s", mBuffer);
                return (false);
            }
            pOssInfoFileData = (OssInfoFileData *)appFileBuffer;
            sprintf(mBuffer, "maxEventEntries = %d, maxBlacklistEntries = %d", pOssInfoFileData->maxEventEntries, pOssInfoFileData->maxBlacklistEntries);
            PrintLnDebug(mBuffer, __LINE__);

            if ((pOssInfoFileData->maxBlacklistEntries == 10) || (pOssInfoFileData->maxBlacklistEntries == 3))
            {
                if (!DesfireCardOpenApp(MASTER_KEY_INDEX, AUTH_KEY_MASTER))
                {
                    sprintf(resultStr, "102,Error opening App: 0x%x with Key: %d", MASTER_KEY_INDEX, AUTH_KEY_MASTER);
                    return false;
                }
                if (!DESFire_DeleteApplication(CRYPTO_ENV0, OSS_SIPORT_APP))
                {
                    sprintf(resultStr, "103,Error Deleting App: 0x%x", OSS_SIPORT_APP);
                    return (false);
                }

                sprintf(mBuffer, "App 0x%x deleted", OSS_SIPORT_APP);
                PrintLnDebug(mBuffer, __LINE__);
            }
            else
            {
                sprintf(mBuffer, "App already on Card and no update necessary");
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "701,%s", mBuffer);
                return (true);
            }
            CopyBytes(ossInfoFileData->cardId, pOssInfoFileData->cardId, sizeof(ossInfoFileData->cardId));
        }
        else // CheckForApp -> No OSS App found get bwCardNumber from App
        {
            if (CheckForApp(BWCARD_OLD_APP))
            {
                byteBufferLen = sizeof(bwCardNumber);
                FillBytes((byte *)bwCardNumber, 0, byteBufferLen);
                if (!ReadDesFireCardAppFile(BWCARD_OLD_APP, BWCARD_DESFIRE_APP_FILEID, (byte *)bwCardNumber, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
                {
                    sprintf(mBuffer, "Error: Action: %d, Reading  App 0x:%x failed: %s", action, BWCARD_OLD_APP, resultStr);
                    PrintLnDebug(mBuffer, __LINE__);
                    sprintf(resultStr, "703,%s", mBuffer);
                    return (false);
                }
                sprintf(mBuffer, "app 0x%x found, cardNumber read: %s", BWCARD_OLD_APP, bwCardNumber);
                PrintLnDebug(mBuffer, __LINE__);
            }
            else if (CheckForApp(KIT_CARD_APP))
            {
                byteBufferLen = sizeof(bwCardNumber);
                FillBytes((byte *)bwCardNumber, 0, byteBufferLen);
                if (!ReadDesFireCardAppFile(KIT_CARD_APP, BWCARD_DESFIRE_APP_FILEID, (byte *)bwCardNumber, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
                {
                    sprintf(mBuffer, "Error: Action: %d, Reading  App 0x:%x failed: %s", action, KIT_CARD_APP, resultStr);
                    PrintLnDebug(mBuffer, __LINE__);
                    sprintf(resultStr, "705,%s", mBuffer);
                    return (false);
                }
                sprintf(mBuffer, "app 0x%x found, cardNumber read: %s", KIT_CARD_APP, bwCardNumber);
                PrintLnDebug(mBuffer, __LINE__);
            }
            else
            {
                sprintf(mBuffer, "Error: Action: %d, no App for CardNumber found", action);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "706,%s", mBuffer);
                return (false);
            }
            decStr2Bytes(bwCardNumber, ossInfoFileData->cardId, sizeof(ossInfoFileData->cardId));
        }
        if (!CreateApp(MASTER_KEY_INDEX, OSS_SIPORT_APP, KIT, resultStr))
        {
            sprintf(mBuffer, "Error: Action: %d, App: 0x%x not created with message: %s", action, OSS_SIPORT_APP, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "708, %s", mBuffer);
            return (false);
        }
        ossInfoFileData->majorVersion = 1;
        ossInfoFileData->minorVersion = 1;
        ossInfoFileData->maxEventEntries = OSS_SIPORT_DESFIRE_APP_INFO_MAX_EVENTLIST;
        ossInfoFileData->maxBlacklistEntries = OSS_SIPORT_DESFIRE_APP_INFO_MAX_BLACKLIST;
        if (!WriteDesFireCardAppFile(OSS_SIPORT_APP, 0, (byte *)ossInfoFileData, sizeof(*ossInfoFileData), 0, 1, KIT, resultStr))
        {
            sprintf(mBuffer, "Error WriteDesFireCardAppFile: %s", resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "709, %s", mBuffer);
            return (false);
        }
        sprintf(resultStr, "0, App: 0x%x created", OSS_SIPORT_APP);
        return (true);
        break;
    case ACTION_READ_OSS_APP_INFO_FILE:
        appId = OSS_SIPORT_APP;
        fileId = OSS_SIPORT_DESFIRE_APP_INFO_FILEID;

        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        if (!CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "711, appId: 0x%x, fileId: %d - file not found", appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }
        FillBytes(appFileBuffer, 0, sizeof(appFileBuffer));
        if (!ReadDesFireCardAppFile(appId, (int)fileId, appFileBuffer, &appFileBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        FillBytes(byteBuffer, 0, sizeof(byteBuffer));

        if (!DESFire_GetUID(CRYPTO_ENV0, byteBuffer, &byteBufferLen, sizeof(byteBuffer)))
        {
            sprintf(resultStr, "02, Error reading UI with app: 0x%x", appId);
            return false;
        }
        CopyBytes((byte *)(byteBuffer + byteBufferLen), appFileBuffer, appFileBufferLen);
        byteBufferLen = byteBufferLen + appFileBufferLen;
        break;

    case ACTION_READ_OSS_APP_DATA_FILE:
        appId = OSS_SIPORT_APP;
        fileId = OSS_SIPORT_DESFIRE_APP_DATA_FILEID;

        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        if (!CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "711, appId: 0x%x, fileId: %d - file not found", appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }
        FillBytes(appFileBuffer, 0, sizeof(appFileBuffer));
        sprintf(mBuffer, "ACTION_READ_OSS_APP_DATA_FILE: sizeof(appFileBuffer)= %d, appFileBufferLen= %d", sizeof(appFileBuffer), appFileBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        if (!ReadDesFireCardAppFile(appId, (int)fileId, appFileBuffer, &appFileBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        pOssDataReadFileData = (OssDataFileData *)appFileBuffer;
        sprintf(mBuffer, "Valid: %02X.%02X.%02X%02X, Index: %02x, DoorInfo:", pOssDataReadFileData->valDateDay, pOssDataReadFileData->valDateMonth, pOssDataReadFileData->valDateYearPart1, pOssDataReadFileData->valDateYearPart2, pOssDataReadFileData->headerDoorIndex);
        PrintLnDebug(mBuffer, __LINE__);
        for (count = 0; count < pOssDataReadFileData->headerDoorIndex; count++)
        {
            position = (pOssDataReadFileData->ossDataDoorInfo[count].DoorId[0] << 8) + pOssDataReadFileData->ossDataDoorInfo[count].DoorId[1];
            sprintf(mBuffer, "%s Group[%d]: %d DTSchedule: %d", mBuffer, count, position, pOssDataReadFileData->ossDataDoorInfo[count].DTSchedule);
        }
        PrintLnDebug(mBuffer, __LINE__);
        sprintf(resultStr, "00,%s", mBuffer);
        break;

    case ACTION_WRITE_OSS_APP_DATA_FILE:
        appId = OSS_SIPORT_APP;
        fileId = OSS_SIPORT_DESFIRE_APP_DATA_FILEID;
        // format of parms: fist byte is length of decoded pamrStr,
        // on debug: if first char is "X" rest is unencrypted
        // else rest of parmStr is encrypted

        byteBufferLen = sizeof(byteBuffer);
        FillBytes(byteBuffer, 0, byteBufferLen);
        byteBuffer2Len = sizeof(byteBuffer2);
        FillBytes(byteBuffer2, 0, byteBuffer2Len);

        parmDataLen = HexStr2Int(parmStr, 2);
        // convert parmStr(HexStr) into bytes in buffer1
#ifdef DEBUG
        if (parmStr[0] == 'X')
        {
            parmStrPtr = &parmStr[1];
            // bwcardNumberEnc just used as parameter strin buffer
            strcpy(bwCardNumberEnc, parmStrPtr);
        }
        else
#endif
        {
            parmStrPtr = &parmStr[2];
            PrintLnDebug(mBuffer, __LINE__);
            HexStr2ByteArray(parmStrPtr, byteBuffer, &byteBufferLen);

            ConvertBinaryToString(byteBuffer, 0, byteBufferLen * 8, outBufferStr, 16, byteBufferLen * 2, byteBufferLen * 2);
            sprintf(mBuffer, "parmStr: %s, parmDataLen: %d, encrypted byteBuffer: %s, bufferLen: %d", parmStrPtr, parmDataLen, outBufferStr, byteBufferLen);
            PrintLnDebug(mBuffer, __LINE__);

            ByteArrayDecrypt(byteBuffer, byteBuffer2, byteBufferLen, (byte *)DATA_KEY);

            ConvertBinaryToString(byteBuffer2, 0, parmDataLen * 8, outBufferStr, 16, parmDataLen * 2, parmDataLen * 2);
            sprintf(mBuffer, "decrypted byteBuffer2 : %s, byteBuffer2Len: %d", outBufferStr, parmDataLen * 2);
            PrintLnDebug(mBuffer, __LINE__);

            // bwcardNumberEnc just used as parameter string buffer
            strcpy(bwCardNumberEnc, (char *)byteBuffer2);
        }

        // bwcardNumberEnc just used as parameter string buffer
        PrintLnDebug(bwCardNumberEnc, __LINE__);
        // check date for starting with 20XX
        if (strncmp(bwCardNumberEnc, "20", 2))
        {
            sprintf(resultStr, "02,Error reading parmStr: %s", bwCardNumberEnc);
            return (false);
        }
        parmStrPtr = strtok(bwCardNumberEnc, "-");
        HexStr2ByteArray(parmStrPtr, byteBuffer, &byteBufferLen);
        ConvertBinaryToString(byteBuffer, 0, byteBufferLen * 8, outBufferStr, 16, byteBufferLen * 2, byteBufferLen * 2);
        sprintf(mBuffer, "parmStrPtr 1: %s, byteBuffer: %s, byteBufferLen: %d", parmStrPtr, outBufferStr, byteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);

        if ((parmStrPtr = strtok(NULL, "-")) != NULL)
        {
            HexStr2ByteArray(parmStrPtr, byteBuffer2, &byteBuffer2Len);
            ConvertBinaryToString(byteBuffer2, 0, byteBuffer2Len * 8, outBufferStr, 16, byteBuffer2Len * 2, sizeof(outBufferStr));
            sprintf(mBuffer, "parmStrPtr 2: %s, byteBuffer2: %s, byteBuffer2Len: %d", parmStrPtr, outBufferStr, byteBuffer2Len);
            PrintLnDebug(mBuffer, __LINE__);
        }

        FillBytes(appFileWriteBuffer, 0, sizeof(appFileWriteBuffer));
        appFileWriteBufferLen = sizeof(appFileWriteBuffer);
        FillBytes(appFileBuffer, 0, sizeof(appFileBuffer));
        appFileBufferLen = sizeof(appFileBuffer);
        sprintf(mBuffer, "ACTION_WRITE_OSS_APP_DATA_FILE: byteBufferLen= %d, appFileBufferLen= %d", byteBufferLen, appFileBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        ClearAppGlobal();
        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        // read current data from card
        if (!ReadDesFireCardAppFile(appId, (int)fileId, appFileBuffer, &appFileBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        else
        {
            PrintLnDebug("end of ReadDesFireCardAppFile", __LINE__);
        }
        pOssDataReadFileData = (OssDataFileData *)appFileBuffer;
        sprintf(mBuffer, "read from card: ValidTo: %02X.%02X.%02X%02X, Index: %02x, DoorInfo:", pOssDataReadFileData->valDateDay, pOssDataReadFileData->valDateMonth, pOssDataReadFileData->valDateYearPart1, pOssDataReadFileData->valDateYearPart2, pOssDataReadFileData->headerDoorIndex);
        PrintLnDebug(mBuffer, __LINE__);

        // get doorInfos of card into appFileWriteBuffer
        if (pOssDataReadFileData->headerDoorIndex > 0)
        {
            ossDoorInfoLen = sizeof(OssDataDoorInfo);
            for (count = 0; count < pOssDataReadFileData->headerDoorIndex; count++)
            {
                appFileWriteBuffer[count * ossDoorInfoLen] = pOssDataReadFileData->ossDataDoorInfo[count].DoorId[0];
                appFileWriteBuffer[count * ossDoorInfoLen + 1] = pOssDataReadFileData->ossDataDoorInfo[count].DoorId[1];
                appFileWriteBuffer[count * ossDoorInfoLen + 2] = pOssDataReadFileData->ossDataDoorInfo[count].DTSchedule;
                ossDoorId = (appFileWriteBuffer[count * ossDoorInfoLen] << 8) + appFileWriteBuffer[count * ossDoorInfoLen + 1];
                sprintf(mBuffer, "%s Group[%d]: %d DTSchedule: %d", mBuffer, count, ossDoorId, pOssDataReadFileData->ossDataDoorInfo[count].DTSchedule);
                // sprintf(mBuffer,"DTSchedule: %d", appFileWriteBuffer[count * 2 + 2]);
                // PrintLnDebug(mBuffer, __LINE__);
            }
            appFileWriteBufferLen = count * ossDoorInfoLen;
            ConvertBinaryToString(appFileWriteBuffer, 0, appFileWriteBufferLen * 8, outBufferStr, 16, appFileWriteBufferLen * 2, sizeof(outBufferStr));
            sprintf(mBuffer, "%s,found Doorinfo: %s, appFileWriteBufferLen: %d, count: %d", mBuffer, outBufferStr, appFileWriteBufferLen, count);
        }
        else
        {
            sprintf(mBuffer, "No DoorIndex on Card");
            appFileWriteBufferLen = 0;
        }
        PrintLnDebug(mBuffer, __LINE__);
        // merge Doorinfo from card (in appFileWriteBuffer) and requested DoorInfo (in byteBuffer) considering Doorinfo to be deleted (in byteBuffer2)
        // result in byteBuffer
        mergeDoorInfo((byte *)&ossDataDoorInfoBuffer, &ossDataDoorInfoIndex, (byte *)(byteBuffer + 6), byteBufferLen - 6, appFileWriteBuffer, appFileWriteBufferLen, byteBuffer2, byteBuffer2Len);
        FillBytes(appFileWriteBuffer, 0, sizeof(appFileWriteBuffer));
        // pOssDataReadFileData = (OssDataFileData *)byteBuffer;
        pOssDataWriteFileData = (OssDataFileData *)appFileWriteBuffer;
        pOssDataWriteFileData->valDateYearPart1 = byteBuffer[0];
        pOssDataWriteFileData->valDateYearPart2 = byteBuffer[1];
        pOssDataWriteFileData->valDateMonth = byteBuffer[2];
        pOssDataWriteFileData->valDateDay = byteBuffer[3];
        pOssDataWriteFileData->valDateHour = byteBuffer[4];
        pOssDataWriteFileData->valDateMinute = byteBuffer[5];

        pOssDataWriteFileData->siteId[0] = pOssDataReadFileData->siteId[0];
        if (pOssDataWriteFileData->siteId[1] == 0x0)
        {
            pOssDataWriteFileData->siteId[1] = 0x1;
        }
        else
        {
            pOssDataWriteFileData->siteId[1] = pOssDataReadFileData->siteId[1];
        }
        pOssDataWriteFileData->headerDTSchedule = pOssDataReadFileData->headerDTSchedule;
        pOssDataWriteFileData->headerDoorIndex = 0;

        // write DoorInfo to appFileWriteBuffer
        ossDataDoorInfoIndex = ossDataDoorInfoIndex / sizeof(OssDataDoorInfo);
        for (count = 0; count < ossDataDoorInfoIndex; count++)
        {
            memcpy((pOssDataWriteFileData->ossDataDoorInfo + pOssDataWriteFileData->headerDoorIndex), &ossDataDoorInfoBuffer[count], sizeof(OssDataDoorInfo));
            (pOssDataWriteFileData->headerDoorIndex)++;
        }
        // count = (byteBufferLen - 6) / 2;
        // for (position = 0; position < count; position++)
        // {
        //     ossDoorInfo.DoorId[0] = byteBuffer[6 + position * 2];
        //     ossDoorInfo.DoorId[1] = byteBuffer[6 + position * 2 + 1];
        //     memcpy((pOssDataWriteFileData->ossDataDoorInfo + pOssDataWriteFileData->headerDoorIndex), &ossDoorInfo, sizeof(OssDataDoorInfo));
        //     (pOssDataWriteFileData->headerDoorIndex)++;
        // }
        // copy DTSchedule to appFileWriteBuffer
        position = (byte *)pOssDataWriteFileData->ossDataDoorInfo - (byte *)pOssDataWriteFileData + pOssDataWriteFileData->headerDoorIndex * sizeof(OssDataDoorInfo);
        pOssPtr = (byte *)(pOssDataWriteFileData);
        pOssPtr += position;
        ConvertBinaryToString((byte *)(pOssDataReadFileData->ossDataDoorInfo + pOssDataReadFileData->headerDoorIndex), 0, 100 * 8, outBufferStr, 16, 100 * 2, 100 * 2);
        sprintf(mBuffer, "read DTS: %s,headerDoorIndex=%d, appFileWriteBufferLen = %d, ", outBufferStr, pOssDataWriteFileData->headerDoorIndex, appFileWriteBufferLen);
        PrintLnDebug(mBuffer, __LINE__);
        memcpy(pOssPtr, (byte *)(pOssDataReadFileData->ossDataDoorInfo + pOssDataReadFileData->headerDoorIndex), sizeof(appFileBuffer) - position);
        // // Delete overlapping doorInfo if more read than to write
        // appFileWriteBufferLen = 16 + sizeof(OssDataDoorInfo) * pOssDataWriteFileData->headerDoorIndex;
        // if (appFileWriteBufferLen < (16 + sizeof(OssDataDoorInfo) * pOssDataReadFileData->headerDoorIndex))
        // {
        //     appFileWriteBufferLen = 16 + sizeof(OssDataDoorInfo) * pOssDataReadFileData->headerDoorIndex;
        // }
        appFileWriteBufferLen = appFileBufferLen - 80;
        ConvertBinaryToString(appFileWriteBuffer, 0, appFileWriteBufferLen * 8, outBufferStr, 16, appFileWriteBufferLen * 2, appFileWriteBufferLen * 2);
        sprintf(mBuffer, "appStr to write: headerDoorIndex=%d, appFileWriteBufferLen = %d, %s", pOssDataWriteFileData->headerDoorIndex, appFileWriteBufferLen, outBufferStr);
        PrintLnDebug(mBuffer, __LINE__);
        PrintLnDebug("END OF DEBUG", __LINE__);
        // sprintf(resultStr, "*********************END OF TEST********************");
        // return (false);

        if (!WriteDesFireCardAppFile(appId, fileId, appFileWriteBuffer, appFileWriteBufferLen, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "05,App 0x%x no write with Message: %s", appId, resultStr);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }
        else
        {
            memset(mBuffer, 0, sizeof(mBuffer));
            strncpy(mBuffer, outBufferStr, 12);
            sprintf(resultStr, "00,%s", mBuffer);
        }

        break;
    case ACTION_WRITE_DORMA_KABA_CARD_ID:
        appId = DORMA_KABA_APP;
        fileId = DORMA_KABA_APP_IDFILE_ID;
        byteBufferLen = sizeof(byteBuffer);
        FillBytes(byteBuffer, 0, byteBufferLen);
        PrintLnDebug("ACTION_WRITE_DORMA_KABA_CARD_ID", __LINE__);
        if (!DesfireCardOpenApp(appId, AUTH_KEY_MASTER))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        if (!CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "711, appId: 0x%x, fileId: %d - file not found", appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }
        if (!ReadDesFireCardAppFile(appId, fileId, byteBuffer, &byteBufferLen, 0, 0, AUTH_KEY_MASTER, UST, resultStr))
        {
            sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        break;
    case ACTION_PHFR_CHANGE_FIRMA:
        appId = IC05_APP;
        fileId = IC05_FILE_OEDT;
        byteBuffer[0] = 0x06;
        byteBufferLen = 1;
        PrintLnDebug("Action 80 entered", __LINE__);
        if (!DesfireCardOpenApp(appId, AUTH_KEY_OTHER_1))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_OTHER_1);
            return (false);
        }
        if (!CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "711, appId: 0x%x, fileId: %d - file not found", appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }
        if (!WriteDesFireCardAppFile(appId, fileId, byteBuffer, byteBufferLen, 1, AUTH_KEY_OTHER_1, PHFR, resultStr))
        {
            sprintf(mBuffer, "05,App 0x%x no write with Message: %s", appId, resultStr);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }
        else
        {
            appId = BWCARD_OLD_APP;
            if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
            {
                sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
                return (false);
            }
            // cardNumber lesen und in bwCardNumber speichern
            if (!ReadDesFireCardAppFile(appId, BWCARD_DESFIRE_APP_FILEID, (byte *)bwCardNumber, &byteBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
            {
                sprintf(mBuffer, "124,Error: Action: %d, Reading  App 0x:%x failed: %s", action, appId, resultStr);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "%s", mBuffer);
                return (false);
            }
        }

        sprintf(resultStr, "00,%s", bwCardNumber);

        break;
    case ACTION_READ_FILE_DATA:
        appId = HexStr2Int(parmStr, APPID_LEN);
        fileId = HexStr2Int(parmStr + APPID_LEN, 2);
        position = HexStr2Int(parmStr + APPID_LEN + 2, 2);
        count = HexStr2Int(parmStr + APPID_LEN + 4, 2);

        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        if (!CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "711, appId: 0x%x, fileId: %d - file not found", appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }
        FillBytes(appFileBuffer, 0, sizeof(appFileBuffer));
        if (!ReadDesFireCardAppFile(appId, (int)fileId, appFileBuffer, &appFileBufferLen, count, position, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }

        break;
#ifndef NO_SECURE
    case ACTION_LOGIN_DATA:
        // tbd read keyCard
        FillBytes((byte *)cardKeyStr, 0, sizeof(cardKeyStr));
        ConvertBinaryToString(CARD_KEY, 0, KEY_LEN * 8, cardKeyStr, 16, 0, sizeof(cardKeyStr));
        ReadDeviceUid(deviceUid);
        FillBytes((byte *)bwCardNumberEnc, 0, sizeof(bwCardNumberEnc));
        sprintf(bwCardNumberEnc, "%s,%s", deviceUid, cardKeyStr);
        byteBufferLen = StrEncrypt2Byte(bwCardNumberEnc, byteBuffer);
        FillBytes((byte *)bwCardNumberEnc, 0, sizeof(bwCardNumberEnc));
        ConvertBinaryToString(byteBuffer, 0, byteBufferLen * 8, bwCardNumberEnc, 16, byteBufferLen * 2, byteBufferLen * 2);
        sprintf(mBuffer, "enCrypted: count: %d,str: %s", byteBufferLen, bwCardNumberEnc);
        PrintLnDebug(mBuffer, __LINE__);
        sprintf(resultStr, "00,%s", bwCardNumberEnc);

        break;
#endif // NO_SECURE
    case ACTION_WAIT_FOR_CARD:
        sprintf(resultStr, "00,card found");
        break;
    default:
        sprintf(resultStr, "01,Error: Unknown action: %d", action);
        return (false);
    }

    sprintf(mBuffer, "DoDesFireCardAction result: %s", resultStr);
    PrintLnDebug(mBuffer, __LINE__);
    return (true);
}
#ifndef NO_SECURE
int DoDesfireSecureAction(int action, char *parmStr, char *resultStr)
{
    int i;
    byte buffer1[PARM_BUFFER_LEN / 2];
    byte buffer2[PARM_BUFFER_LEN / 2];
    int buffer1Len = sizeof(buffer1);
    int buffer2Len = sizeof(buffer2);
    int parmDataLen;

    // Card Appplication Data
    int appId;
    int appIdCount;
    byte fileIdsList[32];
    int fileId = -1;
    int fileIdFound = false;
    int fileIdCount = -1;
    int fileSize = 0;
    byte appFileBuffer[PARM_BUFFER_LEN / 2];
    int appFileBufferLen = sizeof(appFileBuffer);

    sprintf(mBuffer, "DoDesfireCardSecureAction entered, action: %d, parmStr: %s", action, parmStr);
    PrintLnDebug(mBuffer, __LINE__);
    parmDataLen = HexStr2Int(parmStr, 2);

    // convert parmStr(HexStr) into bytes in buffer1
    if (!HexStr2ByteArray(parmStr + 2, buffer1, &buffer1Len))
    {
        PrintError("02,Error in converting Parm String", resultStr);
        return false;
    }

    FillBytes(buffer2, 0, buffer2Len);
    // Decrypt parm in buffer 1 -> buffer2
    if (action == ACTION_SECURE_INIT)
    {
        CopyBytes(secureToken, CRYPT_KEY, KEY_LEN);
    }
    sprintf(mBuffer, "decrypting with token");
    PrintDebug(mBuffer);
    ConvertBinaryToString(secureToken, 0, 16 * 8, mBuffer, 16, 0, 100);
    PrintLnDebug(mBuffer, __LINE__);
    ByteArrayDecrypt(buffer1, buffer2, buffer1Len, secureToken);

    // buffer2 contains decrypted parmStr parmDataLen contains Number of valid Data in buffer2
    ConvertBinaryToString(buffer2, 0, buffer1Len * 8, outBufferStr, 16, 0, sizeof(outBufferStr));
    sprintf(mBuffer, "decrypted ParmStr :%s, bufferLen: %d, validDataLen: %d", outBufferStr, buffer2Len, parmDataLen);
    PrintLnDebug(mBuffer, __LINE__);

    // Check for new Token and copy in secureToken for enCrypt an next deCrypt
    if (CompBytes(buffer2, secureToken, KEY_LEN))
    {
        sprintf(resultStr, "02,secureToken Error");
    }
    else
    {
        CopyBytes(secureToken, buffer2, KEY_LEN);
    }

    sprintf(mBuffer, "token in Buffer2");
    PrintDebug(mBuffer);
    ConvertBinaryToString(buffer2, 0, 16 * 8, mBuffer, 16, 0, 100);
    PrintLnDebug(mBuffer, __LINE__);

    sprintf(mBuffer, "new encrypt token");
    PrintDebug(mBuffer);
    ConvertBinaryToString(secureToken, 0, 16 * 8, mBuffer, 16, 0, 100);
    PrintLnDebug(mBuffer, __LINE__);

    FillBytes(buffer1, 0, sizeof(buffer1));
    buffer1Len = 0;

    if (parmDataLen > 0)
    {
        CopyBytes(buffer1, buffer2 + KEY_LEN, parmDataLen);
        buffer1Len = parmDataLen;
    }

    // buffer1 contains parmData, buffer2 is used for result
    ConvertBinaryToString(buffer1, 0, 8 * buffer1Len, outBufferStr, 16, 2 * buffer1Len, 2 * buffer1Len);
    sprintf(mBuffer, "DoDesfireSecureAction: ParmData in Hex=%s, bufferLen=%d, parmDataLen=%d", outBufferStr, buffer1Len, parmDataLen);
    PrintLnDebug(mBuffer, __LINE__);

    switch (action)
    {
    case ACTION_SECURE_INIT:
        FillBytes(buffer2, 0, sizeof(buffer2));
        GetDeviceUID(buffer2);
        buffer2Len = DEVICE_UID_LEN;
        parmDataLen = buffer2Len;
        break;

    case ACTION_SECURE_GET_UID:
        appId = Hex2Int(buffer1, APPID_LEN);

        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        if (DESFire_GetUID(CRYPTO_ENV0, buffer2, &buffer2Len, sizeof(buffer2)))
        {
            ConvertBinaryToString(buffer2, 0, buffer2Len * 8, outBufferStr, 16, buffer2Len * 2, sizeof(outBufferStr));
            sprintf(mBuffer, "UID:%s, bufferLen: %d", outBufferStr, buffer2Len);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "0,%02x%s", buffer2Len, outBufferStr);
            return true;
        }
        else
        {
            sprintf(resultStr, "02, Error reading UI with app: 0x%x", appId);
            return false;
        }

        break;
    case ACTION_SECURE_READ_FILE:
        // Fill Buffer2 with token+result
        // buffer1 contains parmData

        appId = Hex2Int(buffer1, APPID_LEN);
        fileId = Hex2Int(buffer1 + APPID_LEN, 2);
        sprintf(mBuffer, "Entering ACTION_SECURE_READ_FILE: appId = 0x%x, fileid = %d", appId, fileId);
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();
        sprintf(mBuffer, "Entering ACTION_SECURE_READ_FILE after ChangeSettings: appId = 0x%x, fileid = %d", appId, fileId);
        PrintLnDebug(mBuffer, __LINE__);
        if (!DesfireCardOpenApp(appId, AUTH_KEY_READ))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_READ);
            return (false);
        }
        if (!DESFire_GetFileIDs(CRYPTO_ENV0, fileIdsList, &fileIdCount, sizeof(fileIdsList)))
        {
            sprintf(resultStr, "02,Error Reading  File IDs in App: 0x%x", appId);
            return (false);
        }
        for (i = 0; i < fileIdCount; i++)
        {
            if (fileId == (int)(fileIdsList[i]))
                fileIdFound = true;
        }
        if (!fileIdFound)
        {
            sprintf(resultStr, "02,Error: File: %d, App 0x:%x not found", fileId, appId);
            PrintLnDebug(resultStr, __LINE__);
            return (false);
        }
        sprintf(mBuffer, "Number of fileIds: %d for App: 0x%x", fileIdCount, appId);
        PrintLnDebug(mBuffer, __LINE__);

        FillBytes(appFileBuffer, 0, sizeof(appFileBuffer));
        if (!ReadDesFireCardAppFile(appId, (int)fileId, appFileBuffer, &appFileBufferLen, 0, 0, AUTH_KEY_READ, KIT, resultStr))
        {
            sprintf(mBuffer, "02,Error reading File: AppId: 0x%x, fileId: %d with message:%s", appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }

        FillBytes(buffer2, 0, sizeof(buffer2));

        if (!DESFire_GetUID(CRYPTO_ENV0, buffer2, &buffer2Len, sizeof(buffer2)))
        {
            sprintf(resultStr, "02, Error reading UID with app: 0x%x", appId);
            return false;
        }
        CopyBytes((byte *)(buffer2 + buffer2Len), appFileBuffer, appFileBufferLen);
        buffer2Len = buffer2Len + appFileBufferLen;
        parmDataLen = buffer2Len;
        break;

    case ACTION_SECURE_WRITE_FILE:
        appId = Hex2Int(buffer1, APPID_LEN);
        fileId = Hex2Int(buffer1 + APPID_LEN, FILEID_LEN);
        appFileBufferLen = parmDataLen - APPID_LEN - FILEID_LEN;
        CopyBytes(appFileBuffer, (buffer1 + APPID_LEN + FILEID_LEN), appFileBufferLen);
        sprintf(mBuffer, "Writing App:0x%x,File:%d, DataLen:%d Data:%s", appId, fileId, appFileBufferLen, (char *)appFileBuffer);
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();

        if (!CheckForApp(appId))
        {
            sprintf(resultStr, "02,App 0x%x not on card", appId);
            return false;
        }
        if (!WriteDesFireCardAppFile(appId, fileId, appFileBuffer, appFileBufferLen, 0, AUTH_KEY_WRITE, KIT, resultStr))
        {
            sprintf(mBuffer, "05,App 0x%x no write with Message: %s", appId, resultStr);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }

        buffer2Len = 0;
        parmDataLen = 0;

        break;
    case ACTION_SECURE_CREATE_APP_FILE:
        appId = Hex2Int(buffer1, APPID_LEN);
        fileId = Hex2Int(buffer1 + APPID_LEN, FILEID_LEN);
        fileSize = Hex2Int(buffer1 + APPID_LEN + FILESIZE_LEN, FILEID_LEN);
        sprintf(mBuffer, "%3d001,Create App File: appid=0x%x; fileId=%d; fileSize=0x%x", action, appId, fileId, fileSize);
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();
        if (!CheckForApp(appId))
        {
            sprintf(mBuffer, "%3d102,App not found: appId=0x%x", action, appId);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }
        if (CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "%3d103,App File found, not created: appId=0x%x; fileId=%d", action, appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }
        if (!CreateAppStdFile(appId, fileId, DESF_FILETYPE_STDDATAFILE, DESF_COMMSET_FULLY_ENC, 0x1220, fileSize, KIT, resultStr))
        {
            sprintf(mBuffer, "%d_03, AppId: 0x%x, fileId: %d - File not created: %s", action, appId, fileId, resultStr);
            PrintLnDebug(mBuffer, __LINE__);
            return (false);
        }

        buffer2Len = 0;
        parmDataLen = 0;

        break;
    case ACTION_SECURE_CHECK_FOR_APP:
        appId = Hex2Int(buffer1, APPID_LEN);
        sprintf(mBuffer, "Check for App:0x%x", appId);
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();
        if (!CheckForApp(appId))
        {
            sprintf(resultStr, "05,App 0x%x not found", appId);
            return false;
        }
        buffer2Len = 0;
        parmDataLen = 0;
        break;
    case ACTION_SECURE_CHECK_FOR_APP_FILE:
        appId = Hex2Int(buffer1, APPID_LEN);
        fileId = Hex2Int(buffer1 + APPID_LEN, FILEID_LEN);
        sprintf(mBuffer, "%3d001,Check for App File: appid=0x%x; fileId=%d", action, appId, fileId);
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();
        if (!CheckForApp(appId))
        {
            sprintf(mBuffer, "%3d102,App not found: appId=0x%x", action, appId);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }
        if (!CheckForAppFile(appId, fileId))
        {
            sprintf(mBuffer, "%3d103,App File not found: appId=0x%x; fileId=%d", action, appId, fileId);
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return false;
        }
        buffer2Len = 0;
        parmDataLen = 0;
        break;
    case ACTION_SECURE_LIST_APPS:
        sprintf(mBuffer, "Entering Action 112 listApps");
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();
        appId = MASTER_KEY_INDEX;
        if (!DesfireCardOpenApp(appId, AUTH_KEY_MASTER))
        {
            sprintf(resultStr, "02,Error opening App: 0x%x with Key: %d", appId, AUTH_KEY_MASTER);
            return (false);
        }

        FillBytes(buffer2, 0, sizeof(buffer2));
        if (!ReadDesFireAppIds(buffer2, &buffer2Len, &appIdCount, resultStr))
        {
            sprintf(mBuffer, "03,Error reading AppIds:");
            PrintLnDebug(mBuffer, __LINE__);
            sprintf(resultStr, "%s", mBuffer);
            return (false);
        }
        parmDataLen = buffer2Len;
        break;
    case ACTION_SECURE_CREATE_APPS:
        appId = Hex2Int(buffer1, APPID_LEN);
        sprintf(mBuffer, "Code App:0x%x", appId);
        PrintLnDebug(mBuffer, __LINE__);
        ChangeSettings();
        for (i = 0; parmDataLen - (i * APPID_LEN) > 0; i++)
        {
            appId = Hex2Int(buffer1 + i * APPID_LEN, APPID_LEN);
            sprintf(mBuffer, "creating App: 0x%x", appId);
            PrintLnDebug(mBuffer, __LINE__);
            if (CheckForApp(appId))
            {
                sprintf(resultStr, "05,App 0x%x already found, nothing to create", appId);
                return false;
            }
            if (!CreateApp(MASTER_KEY_INDEX, appId, KIT, resultStr))
            {
                sprintf(mBuffer, "05,App 0x%x not created with message: %s", appId, resultStr);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "%s", mBuffer);
                return false;
            }
        }
        FillBytes(buffer2, 0, sizeof(buffer2));
        buffer2Len = 0;
        parmDataLen = 0;
        break;
    case ACTION_SECURE_DELETE_APPS:
        ChangeSettings();
        for (i = 0; parmDataLen - (i * APPID_LEN) > 0; i++)
        {
            appId = Hex2Int(buffer1 + i * APPID_LEN, APPID_LEN);
            sprintf(mBuffer, "deleting App: 0x%x", appId);
            PrintLnDebug(mBuffer, __LINE__);
            if (!CheckForApp(appId))
            {
                sprintf(resultStr, "05,App 0x%x not found, nothing to delete", appId);
                return false;
            }

            if (!DeleteApp(MASTER_KEY_INDEX, appId, KIT, resultStr))
            {
                sprintf(mBuffer, "05,App 0x%x not deleted with message: %s", appId, resultStr);
                PrintLnDebug(mBuffer, __LINE__);
                sprintf(resultStr, "%s", mBuffer);
                return (false);
            }
            sprintf(resultStr, "0,App 0x%x deleted", appId);
            PrintLnDebug(resultStr, __LINE__);
        }
        FillBytes(buffer2, 0, sizeof(buffer2));
        buffer2Len = 0;
        parmDataLen = 0;
        break;
    }
    // action Funtion End

    // buffer2 contains result if there is one, Length of buffer2 must be on boudary 16
    if (parmDataLen > 0)
    {
        if (buffer2Len % KEY_LEN > 0)
        {
            buffer2Len = (buffer2Len + (KEY_LEN - (buffer2Len % KEY_LEN)));
        }

        ConvertBinaryToString(buffer2, 0, buffer2Len * 8, outBufferStr, 16, buffer2Len * 2, buffer2Len * 2);
        sprintf(mBuffer, "toEncrypt:%s, bufferLen: %d", outBufferStr, buffer2Len);
        PrintLnDebug(mBuffer, __LINE__);
        FillBytes(buffer1, 0, buffer1Len);
        buffer1Len = 0;
        ByteArrayEncrypt(buffer2, buffer1, buffer2Len, secureToken);
        sprintf(mBuffer, "buffer2Len: %d,buffer1Len:%d", buffer2Len, buffer1Len);
        PrintLnDebug(mBuffer, __LINE__);

        ConvertBinaryToString(buffer1, 0, buffer2Len * 8, outBufferStr, 16, buffer2Len * 2, buffer2Len * 2);
        sprintf(resultStr, "00,%02x%s", parmDataLen, outBufferStr);
    }
    else
    // No Data as Result, just return code
    {
        sprintf(resultStr, "00");
    }
    sprintf(mBuffer, "DoDesfireCardSecureAction exit, action, %d, resultStr: %s", action, resultStr);
    PrintLnDebug(mBuffer, __LINE__);

    return true;
}
#endif // NO_SECURE

int setAction(int action)
{
#ifdef READER_ACTION
    return (READER_ACTION);
#else
    return (action);
#endif
}

int checkForNewCard(void)
{
#ifdef READER_ACTION
    if (ISO14443_4_CheckPresence())
    {
        return (false);
    }
    else
    {
        return (true);
    }
#else
    return (true);
#endif
}
int CardAction(int action, char *parmStr, char *resultBuffer)
{
    byte SAK;
    byte ID[MAXIDBYTES];
    int IDBitCnt;
    int TagType;

    if (checkForNewCard())
    // if (true)
    {
        if (SearchTag(&TagType, &IDBitCnt, ID, sizeof(ID)))
        {
            if (ISO14443A_GetSAK(&SAK))
            {
                switch (SAK)
                {
                case 0x20:
                case 0x24:
                    // 0x20 oder 0x24: DESfire-Karte
                    if (action < ACTION_SECURE_INIT)
                    {
                        if (action == ACTION_CHECK_FOR_CARD)
                        {
                            sprintf(resultBuffer, "0");
                        }
                        else
                        {
                            DoDesFireCardAction(action, parmStr, ID, resultBuffer);
                            ClearAppGlobal();
                        }
                    }
                    else
                    {
#ifndef NO_SECURE
                        DoDesfireSecureAction(action, parmStr, resultBuffer);
                        ClearAppGlobal();
#endif
                    }
                    break;
                default:
                    PrintError("Wrong Card Type", resultBuffer);
                    break;
                }
            }
            else
            {
                PrintError("Unknown Card Type (SAK).", resultBuffer);
            }
            return (true);
        }
        return (false);
    }
    else
    {
        return (false);
    }
}

int main(void)
{
    char *pStr = NULL;

    int action;
#ifndef READER_ACTION
    int readCount;
    byte readBuffer[10];
#endif
    byte deviceUid[12];
    char resultBuffer[MBUFFER_LEN];
    char parmStr[PARM_BUFFER_LEN];
    byte parmByteBuffer[PARM_BUFFER_LEN];

    const byte cfgParams[] = {
        SUPPORT_CONFIGCARD, 1,
        SUPPORT_CONFIGCARD_ON,
        USB_KEYBOARDREPEATRATE, 1, 1,
#ifdef GERMAN_KEYBOARD
        USB_KEYBOARDLAYOUT, 1, USB_KEYBOARDLAYOUT_GERMAN,
#endif
        TLV_END};

    pStr = pStr;
    // Parameter setzen
    SetParameters(cfgParams, sizeof(cfgParams));
    SetParameters(ELATEC_KEY_DATA, sizeof(ELATEC_KEY_DATA));

    SetTagTypes(LFTAGTYPES, HFTAGTYPES & ~BLE_MASK);

    // LEDs initialisieren
    LEDInit(REDLED | GREENLED);
    LEDOff(GREENLED);
    LEDOn(REDLED);

    // Begrüßungs-Piepsen
    StartBeep();

    // Ab jetzt lauter piepsen
    SetVolume(100);

    // Serielle Schnittstelle zurücksetzen
#ifndef READER_ACTION
    ClearUsb();
#endif
    // "vorigen Tag" auf "keinen" setzen:
    action = setAction(NO_ACTION);
    StartLedTimer();
    // Main-Schleife, in der jetzt alles abläuft:

    while (true)
    {

        if (action == NO_ACTION)
        {
            FillBytes((byte *)parmStr, 0, sizeof(parmStr));
            action = ReadAction(action, parmStr, parmByteBuffer);
            LEDOff(REDLED);
            LEDOn(GREENLED);
            StartTimer(50);
            while (!TestTimer())
            {
            }
            LEDOff(GREENLED);
        }
        else
        {
            if (checkForNewCard())
            {
                if (EndLedTimer(LED_BLINK_TIME))
                {
                    LEDOff(REDLED);
                    LEDOn(GREENLED);
                    StartTimer(50);
                    while (!TestTimer())
                    {
                    }
                    LEDOff(GREENLED);
                    StartLedTimer();
                }
            }
            else
            {
                LEDOn(REDLED);
            }
        }
        switch (action)
        {
        case NO_ACTION:
            break;
#ifndef SECURE
        case ACTION_BWCARD_OLD_READ_NUMBER:
        case ACTION_BWCARD_OLD_READ_NUMBER_UID:
        case ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED:
        case ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID:
        case ACTION_BWCARD_OLD_READ_NUMBER_DEVICEUID_CARDUID:
        case ACTION_BWCARD_OLD_READ_NUMBER_ENCRYPTED_DEVICEUID:
        case ACTION_CREATE_OSS_APP:
        case ACTION_READ_OSS_APP_INFO_FILE:
        case ACTION_READ_OSS_APP_DATA_FILE:
        case ACTION_WRITE_OSS_APP_DATA_FILE:
        case ACTION_KIT_CARD_READ_NUMBER:
        case ACTION_KIT_CARD_READ_NUMBER_ENCRYPTED:
        case ACTION_READ_FILE_DATA:
        case ACTION_PHFR_CHANGE_FIRMA:
        case ACTION_WRITE_DORMA_KABA_CARD_ID:
        case ACTION_BWCARD_READ_NUMBER_DEVICEUID_CARDUID:
        case ACTION_BWCARD_READ_NUMBER_ENCRYPTED_DEVICEUID:
        case ACTION_BWCARD_READ_NUMBER_DEVICEUID:
        case ACTION_BWCARD_READ_NUMBER_ENCRYPTED:
        case ACTION_BWCARD_READ_NUMBER:
        case ACTION_BWCARD_OLD_READ_NUMBER_BEEP:
        case ACTION_UID_DEC_MSB:
        case ACTION_UID_HEX_DEC_LSB:
        case ACTION_BWCARD_READ_ALL:
        case ACTION_GOOGLESMARTTAP_READ:
        case ACTION_GOOGLESMARTTAP_BWCARD_READ:
        case ACTION_GOOGLESMARTTAP_READ_WAIT:
        case ACTION_GOOGLESMARTTAP_BWCARD_READ_WAIT:
        case ACTION_BWCARD_OLD_READ_NUMBER_FILE_1:
        case ACTION_BWCARD_OLD_WRITE_FILE_1:
        case ACTION_BWCARD_OLD_UID_READ_NUMBER_FILE_1:
        case ACTION_BWCARD_OLD_3G_CHECK:
        case ACTION_BWCARD_OLD_3G_CHECK_ENCRYPTED:
        case ACTION_BWCARD_OLD_CHECK_FILE_1:

#endif
        // case ACTION_DELETE_APP:
        case ACTION_CHECK_FOR_CARD:
        case ACTION_WAIT_FOR_CARD:
#ifndef NO_SECURE
        case ACTION_SECURE_GET_UID:
        case ACTION_SECURE_READ_FILE:
        case ACTION_SECURE_CREATE_APPS:
        case ACTION_SECURE_DELETE_APPS:
        case ACTION_SECURE_CHECK_FOR_APP:
        case ACTION_SECURE_LIST_APPS:
        case ACTION_SECURE_WRITE_FILE:
        case ACTION_SECURE_CHECK_FOR_APP_FILE:
        case ACTION_SECURE_CREATE_APP_FILE:
#endif // NO_SECURE
#ifndef READER_ACTION
            if (action != ACTION_CHECK_FOR_CARD)
            {
                LEDOn(REDLED);
            }
#endif
            FillBytes((byte *)resultBuffer, 0, sizeof(resultBuffer));
            if (CardAction(action, parmStr, resultBuffer))
            {
                if (strlen(resultBuffer) > 0)
                {
                    PrintLnInfo(resultBuffer);
                    if (action == ACTION_BWCARD_OLD_READ_NUMBER_BEEP)
                    {
                        Beep(60, 1897, 250, 50);
                        // Beep(60, 2249, 350, 0);
                    }
                }
#ifdef READER_ACTION
                if (strlen(resultBuffer) > 0)
                {
                    BeepLow();
                }
                // Beep(100, 1597, 50, 50);
                if ((pStr = strchr((char *)resultBuffer, ',')) != NULL)
                {
                    *pStr = 0x0;
                    if (Dec2Int((byte *)resultBuffer, strlen(resultBuffer)) > 0)
                    {
                        Beep(60, 2249, 250, 50);
                        Beep(60, 2249, 350, 0);
                    }
                    else
                    {
                        Beep(50, 1318, 30, 20);
                        Beep(50, 1397, 30, 20);
                        Beep(30, 1568, 30, 20);
                        Beep(50, 1760, 30, 20);
                        Beep(50, 1975, 30, 20);
                    }
                }

#endif
                action = setAction(NO_ACTION);
            }
            else
            {
#ifndef READER_ACTION
                if (action != ACTION_BWCARD_OLD_3G_CHECK && action != ACTION_BWCARD_OLD_3G_CHECK_ENCRYPTED && action != ACTION_WAIT_FOR_CARD && action != ACTION_GOOGLESMARTTAP_BWCARD_READ_WAIT && action != ACTION_GOOGLESMARTTAP_READ_WAIT)
                {
                    sprintf(mBuffer, "09,no card for action %d", action);
                    PrintLnInfo(mBuffer);
                    action = setAction(NO_ACTION);
                }
                else
                {
                    readCount = GetByteCount(CHANNEL_USB, DIR_IN);
                    if (readCount > 0)
                    {
                        ReadBytes(CHANNEL_USB, readBuffer, readCount);
                        if (*readBuffer == 'x')
                        {
                            sprintf(mBuffer, "09,aborted action: %d", action);
                            PrintLnInfo(mBuffer);
                            action = setAction(NO_ACTION);
                        }
                    }
                }

#endif
            }
            LEDOff(REDLED);
            break;
        case ACTION_LOGIN_DATA:
            LEDOn(REDLED);
            DoDesFireCardAction(action, parmStr, NULL, resultBuffer);
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            LEDOff(REDLED);
            break;
#ifndef NO_SECURE
        case ACTION_SECURE_INIT:
            LEDOn(REDLED);
            DoDesfireSecureAction(action, parmStr, resultBuffer);
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            LEDOff(REDLED);
            break;
#endif
        case ACTION_DEVICEUID:
        case ACTION_DEVICEUID_VERSION:
            LEDOn(REDLED);
            FillBytes(deviceUid, 0, sizeof(deviceUid));
            ReadDeviceUid(mBuffer);
            if (action == ACTION_DEVICEUID_VERSION)
            {
                sprintf(resultBuffer, "%s;%d", mBuffer, READER_VERSION);
            }
            else
            {
                sprintf(resultBuffer, "%s", mBuffer);
            }
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            LEDOff(REDLED);
            break;
        case ACTION_SUCCESS_BEEP:
            StartTimer(LED_RESPONSE_TIME);
            LEDOff(REDLED);
            LEDOn(GREENLED);
            Beep(100, 2249, 250, 50);
            Beep(100, 2249, 350, 0);
            while (!TestTimer())
            {
            }
            LEDOff(GREENLED);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_RET:
            Beep(100, 2249, 250, 50);
            Beep(100, 2249, 350, 0);
            sprintf(resultBuffer, "00");
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_OUT:
            StartTimer(LED_RESPONSE_TIME);
            LEDOff(REDLED);
            LEDOn(GREENLED);
            Beep(100, 2249, 250, 50);
            Beep(100, 2149, 350, 0);
            while (!TestTimer())
            {
            }
            LEDOff(GREENLED);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_OUT_RET:
            Beep(100, 2249, 250, 50);
            Beep(100, 2149, 350, 0);
            sprintf(resultBuffer, "00");
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_3G:
            StartTimer(LED_RESPONSE_TIME);
            LEDOff(REDLED);
            LEDOn(GREENLED);
            Beep(100, 2249, 250, 50);
            Beep(100, 2300, 250, 50);
            Beep(100, 2249, 500, 100);
            Beep(100, 2300, 250, 0);
            while (!TestTimer())
            {
            }
            LEDOff(GREENLED);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_3G_RET:
            Beep(100, 2249, 250, 50);
            Beep(100, 2300, 250, 50);
            Beep(100, 2249, 500, 100);
            Beep(100, 2300, 250, 0);
            sprintf(resultBuffer, "00");
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_WRITE:
            StartTimer(LED_RESPONSE_TIME);
            LEDOff(REDLED);
            LEDOn(GREENLED);
            Beep(60, 2093, 30, 20);
            Beep(60, 2349, 30, 20);
            Beep(60, 2489, 30, 20);
            Beep(60, 2637, 30, 0);
            while (!TestTimer())
            {
            }
            LEDOff(GREENLED);
            action = setAction(NO_ACTION);
            break;
        case ACTION_SUCCESS_BEEP_WRITE_RET:
            Beep(60, 2093, 30, 20);
            Beep(60, 2349, 30, 20);
            Beep(60, 2489, 30, 20);
            Beep(60, 2637, 30, 0);
            sprintf(resultBuffer, "00");
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        case ACTION_ERROR_BEEP:
            StartTimer(LED_RESPONSE_TIME);
            LEDOff(GREENLED);
            LEDBlink(REDLED, 50, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 0);
            while (!TestTimer())
            {
            }
            LEDOff(REDLED);
            action = setAction(NO_ACTION);
            break;
        case ACTION_ERROR_BEEP_RET:
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 50);
            Beep(100, 2400, 80, 0);
            sprintf(resultBuffer, "00");
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        case ACTION_HARD_ERROR_BEEP:
            StartTimer(4 * LED_RESPONSE_TIME);
            LEDOff(GREENLED);
            LEDBlink(REDLED, 50, 50);
            Beep(100, 2400, 250, 100);
            Beep(100, 2400, 250, 100);
            Beep(100, 2400, 250, 0);
            while (!TestTimer())
            {
            }
            action = setAction(NO_ACTION);
            break;
        case ACTION_HARD_ERROR_BEEP_RET:
            Beep(100, 2400, 250, 100);
            Beep(100, 2400, 250, 100);
            Beep(100, 2400, 250, 0);
            sprintf(resultBuffer, "00");
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        case ACTION_VERSION:
            sprintf(resultBuffer, "%d", READER_VERSION);
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
            break;
        default:
            sprintf(resultBuffer, "99,error: No valid action: %d", action);
            PrintLnInfo(resultBuffer);
            action = setAction(NO_ACTION);
        }
        StartTimer(100);
        while (!TestTimer())
        {
        }
    }
}
