@echo off
SETLOCAL
goto :init
:header
    echo %__NAME% v%__VERSION%
    echo generates ELATEC Firmware and loads it.
    echo.
    goto :eof

:usage
    echo USAGE:
    echo   %__BAT_NAME% [flags] "serial|reader"
    echo.
    echo.  -?, --help                   shows this help
    echo.  -v, --version                shows the version
    echo.  -e, --verbose                shows detailed output
    echo.  -d, --debug                  debug messages on
    echo.  -s, --secure                 set secure mode
    echo.  -m, --miniterm "comx"        start miniterm for debug with comx Device
    echo.  -n, --no-load                do not load firmware
    echo. -ek, --english_keyboard       set keyboard emulation mode in reader mode to english (default is german) 
    echo. -sa, --serial_action          reduce serial mode to specified action 
    echo.  -r, --reader_action "action" set the reader action
    echo.                               This option is mandatory in reader mode
    goto :eof

:version
    if "%~1"=="full" call :header & goto :eof
    echo. Make Version: %__VERSION%
    echo. ElATEC dev kit Version: %DEV_VERSION%
    goto :eof

:missing_argument
    call :header
    call :usage
    echo.
    echo ****                                   ****
    echo ****    MISSING "REQUIRED ARGUMENT"    ****
    echo ****                                   ****
    echo.
    goto :eof

:init
    rem Development KIT Version
    set PROJECT=KIT-Card
    set PROJECT_SOURCE_FILE=src/KIT-Card.c
    set "DEV_VERSION=480"
    set "FIRMWARE_VERSION=%DEV_VERSION%"

    set "ELATEC_DIR=C:\Users\xy0007\Documents\KIT-Card\Elatec\TWN4DevPack%DEV_VERSION%"
    set "BIX_FILE=TWN4_CCx%FIRMWARE_VERSION%_KIT100_%PROJECT%.bix"
    set "FIRMWARE_PATH=Firmware\includes"

    set "COMPILER=%ELATEC_DIR%\Tools\Yagarto-20110328\bin\arm-none-eabi-gcc"

    set "__NAME=%~n0"
    set "__VERSION=1.01"

    set "__BAT_FILE=%~0"
    set "__BAT_PATH=%~dp0"
    set "__BAT_NAME=%~nx0"

    set "UnNamedArgument="
    set "UnNamedOptionalArg="
    set "NamedFlag="
    set "CompilerOptions="

:parse
    if "%~1"=="" goto :validate

    if /i "%~1"=="-?"         call :header & call :usage "%~2" & goto :end
    if /i "%~1"=="--help"     call :header & call :usage "%~2" & goto :end

    if /i "%~1"=="-v"         call :version      & goto :end
    if /i "%~1"=="--version"  call :version full & goto :end

    if /i "%~1"=="-e"         set "OptVerbose=yes"  & shift & goto :parse
    if /i "%~1"=="--verbose"  set "OptVerbose=yes"  & shift & goto :parse

    if /i "%~1"=="-d"          set "DebugOption=yes"   & shift & goto :parse
    if /i "%~1"=="--debug"     set "DebugOption=yes"   & shift & goto :parse

    if /i "%~1"=="-s"          set "SecureOption=yes"   & shift & goto :parse
    if /i "%~1"=="--secure"    set "SecureOption=yes"   & shift & goto :parse

    if /i "%~1"=="-mc"          set "MinitermOptionCom=%~2"   & shift & shift & goto :parse
    if /i "%~1"=="--minitermCom"    set "MinitermOptionCom=%~2"   & shift & shift & goto :parse

    if /i "%~1"=="-m"          set "MinitermOption=yes"   & shift & shift & goto :parse
    if /i "%~1"=="--miniterm"    set "MinitermOption=yes"   & shift & shift & goto :parse

    if /i "%~1"=="-n"          set "NoloadOption=yes"   & shift & goto :parse
    if /i "%~1"=="--no-load"    set "NoloadOption=yes"   & shift & goto :parse

    if /i "%~1"=="--no-secure"    set "NoSecureOption=yes"   & shift & goto :parse

    if /i "%~1"=="-r"             set "ReaderAction=%~2"   & shift & shift & goto :parse
    if /i "%~1"=="--reader_action" set "ReaderAction=%~2"   & shift & shift & goto :parse

    if /i "%~1"=="-sa"             set "SerialAction=%~2"   & shift & shift & goto :parse
    if /i "%~1"=="--serial_action" set "SerialAction=%~2"   & shift & shift & goto :parse

    if /i "%~1"=="-ek"               set "eng_keyboard=yes"   & shift & goto :parse
    if /i "%~1"=="--english_keyboard" set "eng_keyboard=yes"   & shift & goto :parse

    if not defined UnNamedArgument     set "UnNamedArgument=%~1"     & shift & goto :parse

    shift
    goto :parse

:validate
    if not defined UnNamedArgument call :missing_argument & goto :end
    if /i "%UnNamedArgument%"=="reader" (
        if not defined ReaderAction (call :missing_argument & goto :end)
    )
        
    

:main
    if defined DebugOption (
        set DEBUG= -DDEBUG
    )
    if defined SecureOption (
        set SECURE= -DSECURE
    )
    if defined NoSecureOption (
        set SECURE= -DNO_SECURE
    )
    if defined ReaderAction (
        set READER_ACTION= -DREADER_ACTION=%ReaderAction%
    )
    if defined SerialAction (
        set SERIAL_ACTION= -DSERIAL_ACTION=%SerialAction%
    )
    if not defined eng_keyboard (
        set GERMAN_KEYBOARD= -DGERMAN_KEYBOARD
    )
    set "CompilerOptions=%DEBUG%%SECURE%%READER_ACTION%%GERMAN_KEYBOARD%"
    
    if /i "%UnNamedArgument%"=="serial" set FIRMWARE_VERSION=C
    if /i "%UnNamedArgument%"=="reader" set FIRMWARE_VERSION=K
    if not defined FIRMWARE_PATH call :missing_argument & goto :end

    if defined OptVerbose (
        echo making with follwoing parameters:
        echo "UnNamedArgument"=%UnNamedArgument%
        echo "FIRMWARE_PATH"=%FIRMWARE_PATH%
        echo "CompilerOptions"=%CompilerOptions%
        echo on
    )
    set COMPILER_PARMSTR=-std=c99 -mcpu=cortex-m0 -Os -ffunction-sections -gdwarf-2 -mthumb -fomit-frame-pointer -Wall -Wstrict-prototypes -fverbose-asm -Wa,-ahlms="App_KITC100_%PROJECT%.lst" -DAPPCHARS=KITC -DVERSION=0x100 -DAPPEXTCONFIG=0 %DEBUG% %SECURE% %SERIAL_ACTION% %READER_ACTION% %GERMAN_KEYBOARD% -I. -I"%ELATEC_DIR%\Tools\sys" "%ELATEC_DIR%\Tools\sys\twn4.crt.c" "%PROJECT_SOURCE_FILE%" -nostartfiles -T"%ELATEC_DIR%\Tools\sys\app.ld" -Wl,--gc-sections,-e,AppHeader,--no-print-gc-sections,-Map="App_KITC100_%PROJECT%.map",--cref,--no-warn-mismatch "%ELATEC_DIR%\Tools\sys\libapp.a" -lc -o "App_KITC100_%PROJECT%.elf"
    %COMPILER% %COMPILER_PARMSTR%
    if errorlevel 1 goto end
        @REM copy "%ELATEC_DIR%\%FIRMWARE_PATH%" TWN4_%DEV_VERSION%.bix
        "%ELATEC_DIR%\Tools\Yagarto-20110328\bin\arm-none-eabi-objcopy" -O ihex "App_KITC100_%PROJECT%.elf" "App_KITC100_%PROJECT%.hex"
    if errorlevel 1 goto end
        @REM "%ELATEC_DIR%\Tools\makeapp" -v4 -tTWN4 -nTWN4   "-iTWN4_CKx405.bix" "-iTWN4_MKx405.bix" "-iTWN4_NKx405.bix" "-hApp_KITC100_%PROJECT%.hex" "-o%BIX_FILE%"
        "%ELATEC_DIR%\Tools\makeapp" -v4 -tTWN4 -nTWN4 "-i%ELATEC_DIR%\%FIRMWARE_PATH%\TWN4_C%FIRMWARE_VERSION%x%DEV_VERSION%.bix" "-i%ELATEC_DIR%\%FIRMWARE_PATH%\TWN4_M%FIRMWARE_VERSION%x%DEV_VERSION%.bix" "-i%ELATEC_DIR%\%FIRMWARE_PATH%\TWN4_N%FIRMWARE_VERSION%x%DEV_VERSION%.bix" "-hApp_KITC100_%PROJECT%.hex" "-o%BIX_FILE%"
    if errorlevel 1 goto end
        del "App_KITC100_%PROJECT%.elf" "App_KITC100_%PROJECT%.lst" "App_KITC100_%PROJECT%.map" "App_KITC100_%PROJECT%.hex" "TWN4_%DEV_VERSION%.bix"
        if not defined NoloadOption (
            "%ELATEC_DIR%\Tools\flash" USB %BIX_FILE%
        )
    if defined MinitermOption (
        timeout /T 2
        python  -m serial.tools.miniterm -e --exit-char 25 
    )
    if defined MinitermOptionCom (
        timeout /T 2
        python  -m serial.tools.miniterm -e --exit-char 25 %MinitermOptionCom%  
    )

    @REM pause 
    :end
    @REM pause 
    exit /B
