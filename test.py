import os
from time import sleep
from serial.tools.list_ports import comports
import binascii
from Crypto.Cipher import AES
READER_KEY = os.getenv("READER_KEY").encode()


def decryptString(str):
    cipher = AES.new(READER_KEY, AES.MODE_ECB)
    try:
        str = binascii.unhexlify(str)
        strDecrypt = cipher.decrypt(str)
        return True, strDecrypt.decode().replace('\x00', '')
    except Exception as e:
        return (False, f"Error {e} on decryptString: {str}")

def serTest():
    serPort = None
    if comports:
        comportsList = list(comports())
        for port in comportsList:
            if port.product is not None and port.product.startswith("TWN4"):
                serPort = port[0]
                break
            if port.manufacturer is not None and port.manufacturer.startswith("Elatec"):
                serPort = port[0]
                break
            if port.hwid is not None and "09D8" in port.hwid:
                serPort = port[0]
                break
        if not serPort:
            print(f"card reader device with not found")
            exit()
    print (serPort)
    os.system(f"python -m serial.tools.miniterm -e --exit-char 25 {serPort}")

if __name__ == "__main__":
    serTest()
    # print(decryptString("E4625C89D1E699B83F57F7827EAF5570"))
