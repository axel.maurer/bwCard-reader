## bwCard Reader  

Voraussetzung zur Nutzung dieser Software ist ein [ELATEC Multitech oder Mifare TWN4](https://www.elatec-rfid.com/fileadmin/Documents/Data-Sheet/DS-TWN4-MultiTech-2-LF-HF-DocRev8.pdf) und das zugehörige [Dev-Kit](https://www.elatec-rfid.com/de-de/elatec-software). Die Kosten für einen Leser liegen bei ca 100 € zzgl MwSt (Stand Juli 2021).
Das Dev-Kit muss installiert sein, der Leser über USB angeschlossen. Das Dev-Kit ist nur unter Windows nutzbar. Die Programmierung des Leser muss damit immer unter Windows erfolgen, auch dann, wenn die Nutzung des Lesers unter Linux erfolgen soll.
Der bwCard Reader kennt zwei Modi:

- reader mode --> USB Tastatur (HID Device, engl. Tastaturlayout)
- serial mode --> Serial device (über VCOM Treiber)

Im "reader mode" reagiert der Leser direkt auf das Auflegen einer Karte und liefert das Ergebnis der Funktion über das HID Device direkt aus. Im "serial mode" werden die Funktionen über die serielle Schnittstelle angestoßen und liefern die Ergebniswerte nach der Ausführung an diese zurück. In beiden Fällen muss der Leser an einen Rechner angeschlossen sein. Ein Stand-Alone Betrieb ist nicht möglich, auch dann nicht, wenn der Leser so programmiert ist, dass er die Funktionen eigenständig ausführt. Die Treiber für beide Varianten werden von Windows und Linux mitgeliefert. Eine Treiber-Installation ist in der Regel nicht erforderlich. Vor dem Übersetzen müssen in `make.bat` die `init`-Parameter entsprechend der lokalen Installation gesetzt werden.<br />
Damit die Datei übersetzt werden kann, müssen zunächst die entsprechenden Lese- und Schreibschlüssel eingetragen werden. Dies geschieht indem man die Datei "keysNull.h" in die Datei keys.h kopiert, dort die entsprechenden Schlüssel einträgt und den Pfad für diese Daten in der Sourcedatei anpasst. Eine Vorlage mit dem bwCard Leseschlüssel ist beim Author erhätlich.
In der Datei `make.bat` muss noch der Pfad für das "ELATEC_DIR" angepasst werden.
Zur Übersetzung und zum Laden der Firmware wird `make.bat` aufgerufen. Folgende Parameter können angegeben werden:

```
USAGE:
  make.bat [flags] "serial|reader"

  -?, --help                   shows this help
  -v, --version                shows the version
  -e, --verbose                shows detailed output
  -d, --debug                  debug messages on
  -s, --secure                 set secure mode
 -ek, --english_keyboard       set keyboard emulation mode in reader mode to english (default is german)
  -r, --reader_action "action" set the reader action
                               This option is mandatory in reader mode
```
> Defines:<br />
> READER_ACTION --> Standardfunktion, die ausgeführt werden soll, wenn eine Karte aufgelegt wird. Dabei ist zu beachten, dass im reader mode nur Funnktionen angegeben werden können, die nur Daten ausgeben.<br />
> SECURE --> Nur im serial mode verfügbar. Damit werden nur Funktionen im SECURE-Mode freigeschaltet (siehe unten). Funktionen im Normalmodus (<100) können nicht mehr ausgeführt werden.<br />
> GERMAN_KEYBOARD --> Nur im reader mode verfügbar. Stellt den Reader auf deutsche Tastatur (Standard ist englische Tastatur).

Im serial mode kennt der Leser folgende Befehle, die über die serielle Schnittstelle an den Leser übergeben werden. Die seriellen Parameter sind:

    baudrate=9600,
    parity=NONE,
    stopbits=ONE,
    bytesize=EIGHTBITS
Unter Windows wird der Leser mit "comx" angesprochen unter Linux unter "/dev/ttyACMx", wobei x der Nummer der Schnittstelle entspricht.
Alle Befehle werden mit `0x0D`abgeschlossen. Zeichen dahinter werden ignoriert.
Alle Angaben sind als Zeichen zu übermitteln, wenn nichts anderes vermerkt ist.
Parameter werden mit ',' getrennt übergeben. z.B. "17,1625364"

Im serial mode werden alle Befehle direkt ausgeführt. Sollte zum Zeitpunkt der Ausführung keine Karte aufliegen, so kommt die Meldung "09,no card for action 1". Das heißt die Anwendung muss selbst die Warteschleife implementieren. Ausnahmen sind die Funktionen 17 und 18. Hier wartete der Reader bis eine Karte aufgelegt wurde. (implementiert aus Performance Gründen)
Fehlermeldungen haben immer folgenden Aufbau: 
`<fehlernummer>,<Erläuterungen zum Fehler>`
|Befehl| App | Return | Parameter
|--|--|--|--|
| 1 | bwCard old (becaad) | bwCard-Nummer |
| 2 | bwCard old (becaad) | bwCard-Nummer;UID |
| 1 | bwCard old (becaad) | bwCard-Nummer with success beep|
|10|bwCard old (becaad)|bwCard-Nummer encrypted (Hex)
|11|bwCard old (becaad)|bwCard-Nummer;readerUID (Hex)
|12|bwCard old (becaad)|cardUID (Hex);readerUID (Hex)
|13|bwCard old (becaad)|bwCard-Nummer encrypted (Hex)>;<readerUID (Hex)
|14|bwCard old (becaad)|special function UKN
|15|bwCard old (becaad)|special function UKN
|16|bwCard old (becaad)|special function UKN
|17|bwCard old (becaad)|bwCard-Nummer;<check for file 1>;True or False|date in sec
|18|bwCard old (becaad)|bwCard-Nummer; encrypted;<check for file 1>;True or False|date in sec
|19|bwCard old (becaad)|<check for file 1>;True or False|date in sec
|20|Success Beep|Kein Returnwert
|21|Success Beep|Kein Returnwert
|22|Special Success Beep|Kein Returnwert
|23|Special Success Beep|Kein Returnwert
|25|Success Beep (20)|Returnwert: 00
|26|Success Beep (21)|Returnwert: 00
|27|Success Beep (22)|Returnwert: 00
|28|Success Beep (23)|Returnwert: 00
|30|Error Beep|Kein Returnwert
|31|Special Error Beep|Kein Returnwert
|35|Error Beep (30)|Returnwert: 00
|36|Special Error Beep (31)|Returnwert: 00
|50|read device UID|readerUID (Hex)
|51|Generate Login Data for secure mode|token for secure mode
|60| bwCard (F58860) | bwCard-Nummer |
|61|bwCard (F58860)|bwCard-Nummer encrypted (Hex)
|62|bwCard (F58860)|bwCard-Nummer;readerUID (Hex)
|63|bwCard (F58860)|cardUID (Hex);readerUID (Hex)
|64|bwCard (F58860)|bwCard-Nummer encrypted (Hex)>;<readerUID (Hex
|65|bwCard (F58860)|bwCard-Nummer;Scope;UID;ESCN
|70|OSS/SIPORT (F51773)|create OSS App  --> 0,app created *)
|71|OSS/SIPORT (F51773)| OSS Info File
|90|liest Google Tap Information
|91|liest Google Tap Information oder bwCardNumber 
|92|liest Google Tap Information -> wartet bis Karte aufgelegt wurde **)
|93|liest Google Tap Information oder bwCardNumber -> wartet bis Karte aufgelegt wurde **)

*)Liest die bwCard Nummer (aus 0xbecaad oder 0xcc1580), legt die OSS App an, falls erforderlich und schreibet die bwCard Nummer in die OSS APP

**) Der Kartenleser wartet intern bis eine Karte aufgelegt wurde oder der Anschluss getrennt wurde.

#### Secure Mode
Zusätzlich zu obigen Funktionen ist noch ein "SECURE-Mode" implementiert. Im Secure Mode folgen die Befehle dem in der Abbildung gezeigten Modus (angelehnt an Cipher Block Chaining Mode (CBC Mode) ).
![coding_sequence](https://www.plantuml.com/plantuml/png/bPF1JiCm38RlUGhV0G4xJW9Dsc0gI81W4qSK6jD5QvsHk1jxUqfQigYBgBXjzkVVzkzcubXQhgiISgCq5MyEFska78MObXg9uUmQfjAgPvGABOmXUyWM8ehqy-sFfLRdIuTs9bdswLPt33UKs_s6KS6133rVq6wzNcaAJKcuGnhuv--xTDZWqnNcQ_WmrdCYP299du77-o0GfUP8Q2KZi5aZNGGa6llkTG4xpIl8519hte_1_kuC9u1Xq4uYxUCn9uJCMMybOzTBbCPiG9DcBKjmw9msbCtWwj3lj2s-R6AT2W10maMpkQsfaJVaYSJc1SQRZoAivx1ZOuUfEJ8yILivZnoV8QwCtVKr84br7IAVlEdjlhuwPUp_5FxuZ1NwebOjBlJA-8LvpHZPEgGNkmRI0U4kFKdiHXYzoRcwv6YEKVhIwPB1QnN8WHawLmZ6DhND1ZwQD1MMjUt06JMljvBUL2Gb5FRVnQG9rrKflW40 "coding_sequence")

Die implementierten Befehle im Secure Mode werden auf Nachfrage beschrieben.

